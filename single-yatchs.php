<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row  no-gutters">
        <div class="back-btn-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <a href="<?php echo home_url('/yatchs'); ?>" title="<?php _e('Go back to Yatch Fleet', 'yam'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-chevron.png" alt="Go Back" class="img-fluid" /> <?php _e('Go back to Yatch Fleet', 'yam'); ?></a>
        </div>
        <div class="page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row justify-content-center">
                    <section class="yatch-single col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                        <?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
                        <picture class="yatch-gallery col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="main-picture col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                                    <?php the_post_thumbnail('full', $defaultatts); ?>
                                </div>
                                <?php $yatchs_gallery = get_post_meta(get_the_ID(), 'yam_yatchs_gallery', true); ?>
                                <div class="secondary-picture col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <?php $i = 1; ?>
                                    <?php foreach ($yatchs_gallery as $key => $value) { ?>
                                    <?php if ($i == 1) { ?>
                                    <div class="secondary-picture-wrapper">
                                        <?php $image = wp_get_attachment_image_src($key, 'boxed'); ?>
                                        <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                    </div>
                                    <?php } ?>

                                    <?php if ($i == 2) { ?>
                                    <div class="smaller-picture">
                                        <?php $image = wp_get_attachment_image_src($key, 'small'); ?>
                                        <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                    </div>
                                    <?php } ?>
                                    <?php if ($i == 3) { ?>
                                    <div class="smaller-picture">
                                        <?php $image = wp_get_attachment_image_src($key, 'small'); ?>
                                        <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                        <div class="smaller-picture-wrapper">
                                            + <?php echo count($yatchs_gallery); ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($i > 3) { ?>
                                    <?php $image = wp_get_attachment_image_src($key, 'full'); ?>
                                    <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid d-none" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                    <?php } ?>
                                    <?php $i++; } ?>
                                </div>

                            </div>

                        </picture>
                        <header>
                            <h1 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h1>
                            <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-guests.png" alt="guests" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_yatchs_guests', true); ?></span>
                        </header>
                        <?php the_content(); ?>
                        <a title="<?php _e('Request Service Quote', 'yam'); ?>" class="btn btn-md btn-concierge" data-toggle="modal" data-target="#quoteModal"><?php _e('Request Service Quote', 'yam'); ?></a>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class=" modal fade" id="quoteModal" tabindex="-1" aria-labelledby="quoteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="quoteModalLabel"><?php _e('Request a Luxury Concierge Service Quote', 'yam'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo get_template_part('templates/template-quote-form'); ?>
            </div>
            <div class="modal-footer">
                <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-phone.png" alt="phone"> 305.527.2367</span>
                <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-email.png" alt="email"> shiraencaoua@gmail.com</span>
                <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-instagram.png" alt="instagram"> </span>
                <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-facebook.png" alt="facebook"> </span>
                <span>@yamvacation</span>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>