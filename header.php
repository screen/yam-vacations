<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
    <meta name="robots" content="NOODP, INDEX, FOLLOW" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="dns-prefetch" href="//facebook.com" crossorigin />
    <link rel="dns-prefetch" href="//connect.facebook.net" crossorigin />
    <link rel="dns-prefetch" href="//ajax.googleapis.com" crossorigin />
    <link rel="dns-prefetch" href="//google-analytics.com" crossorigin />
    <link rel="preconnect" href="//fonts.gstatic.com" crossorigin />
    <?php /* FAVICONS */ ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
    <?php /* THEME NAVBAR COLOR */ ?>
    <meta name="msapplication-TileColor" content="#454545" />
    <meta name="theme-color" content="#454545" />
    <?php /* AUTHOR INFORMATION */ ?>
    <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
    <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
    <?php wp_title('|', false, 'right'); ?>
    <?php wp_head() ?>
</head>

<body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
    <?php wp_body_open(); ?>
    <div id="fb-root"></div>
    <?php $class = (is_front_page()) ? 'container-fluid-home' : ''; ?>
    <header id="headerCntr" class="container-fluid p-0 <?php echo $class; ?>" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        <div class="row no-gutters">
            <?php $header_options = get_option('yam_header_settings'); ?>
            <div class="top-bar col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="container">
                    <div class="row">
                        <div class="top-bar-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php if ($header_options['header_left'] != '') { ?>
                            <div><img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon-white.png" alt="Call Us" class="img-fluid" /> <?php echo $header_options['header_left']; ?> <a href="<?php echo $header_options['header_phone_link']; ?>" title="<?php _e('Call Us', 'yam'); ?>"><?php echo $header_options['header_phone_text']; ?></a></div>
                            <?php } ?>
                            <div><?php echo $header_options['header_right']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="headerMenu" class="the-header col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="container container-special">
                    <div class="row align-items-center">
                        <div class="header-logo col-xl-2 col-lg-2 col-md-3 col-sm-6 col-6">
                            <a class="navbar-brand" href="<?php echo home_url('/'); ?>" title="<?php echo get_bloginfo('name'); ?>">
                                <?php $brightness = (is_front_page()) ? 'logo-bright' : ''; ?>
                                <?php $custom_logo_id = get_theme_mod('custom_logo'); ?>
                                <?php $image = wp_get_attachment_image_src($custom_logo_id, 'logo'); ?>
                                <?php if (!empty($image)) { ?>
                                <img src="<?php echo $image[0]; ?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo <?php echo $brightness; ?>" />
                                <?php } ?>
                            </a>
                        </div>
                        <div id="menuCntr" class="header-menu col-xl-8 col-lg-8 col-md-9 col-sm-12 col-12 order-xl-2 order-lg-2 order-md-2 order-sm-3 order-3 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                            <?php
                            wp_nav_menu(array(
                                'theme_location'  => 'header_menu',
                                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                'container'       => 'div',
                                'menu_class'      => 'custom-navbar-nav'
                            ));
                            ?>
                        </div>
                        <div class="header-mobile col-xl-2 col-lg-2 col-md-3 col-sm-6 col-6 order-xl-3 order-lg-3 order-md-3 order-sm-2 order-2 d-xl-none d-lg-none d-md-none d-sm-block d-block">
                            <button id="menuBtn" class="btn-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>