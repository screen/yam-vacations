<?php get_header(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <div class="page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row">
                    <div class="back-btn-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo home_url('/luxury-concierge-services'); ?>" title="<?php _e('Go back to Luxury Concierge Services'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-chevron.png" alt="Go Back" class="img-fluid" /> <?php _e('Go back to Luxury Concierge Services'); ?></a>
                    </div>
                    <div class="title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1><?php _e('Available Yatch Fleet', 'yam'); ?></h1>
                    </div>
                    <?php if (have_posts()) : ?>
                        <section class="yatch-archive col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
                                <?php while (have_posts()) : the_post(); ?>
                                    <article id="post-<?php the_ID(); ?>" class="archive-item yatch-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 <?php echo join(' ', get_post_class()); ?>" role="article">
                                        <div class="row">
                                            <picture class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <?php if (has_post_thumbnail()) : ?>
                                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                        <?php the_post_thumbnail('full', $defaultatts); ?>
                                                    </a>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                        <img itemprop="image" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img.jpg" alt="No img" class="img-fluid" />
                                                    </a>
                                                <?php endif; ?>
                                            </picture>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <header>
                                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                        <h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                                                    </a>
                                                    <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-guests.png" alt="guests" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_yatchs_guests', true); ?></span>
                                                </header>
                                                <a href="<?php the_permalink(); ?>" title="<?php _e('View More Info', 'yam'); ?>" class="btn btn-md btn-view-more"><?php _e('View More Info', 'yam'); ?></a>
                                            </div>
                                        </div>
                                    </article>
                                <?php endwhile; ?>
                            </div>
                        </section>
                        <div class="pagination col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php if (function_exists('wp_paginate')) {
                                wp_paginate();
                            } else {
                                posts_nav_link();
                                wp_link_pages();
                            } ?>
                        </div>
                    <?php else : ?>
                        <section class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h2><?php _e('Disculpe, su busqueda no arrojo ningun resultado', 'yam'); ?></h2>
                            <h3><?php _e('Dirígete nuevamente al', 'yam'); ?> <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'yam'); ?>"><?php _e('inicio', 'yam'); ?></a>.</h3>
                        </section>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>