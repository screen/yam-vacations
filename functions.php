<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) {
    add_action('wp_enqueue_scripts', 'yam_enqueue_jquery');
}
function yam_enqueue_jquery()
{
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.6.0', false);
        wp_register_script('jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js', array('jquery'), '3.3.2', false);
    } else {
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', false, '3.6.0', false);
        wp_register_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.2.min.js', array('jquery'), '3.3.2', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action('after_setup_theme', 'yam_register_navwalker');
function yam_register_navwalker()
{
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

if (class_exists('WooCommerce')) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */

if (defined('JETPACK__VERSION')) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus(array(
    'header_menu' => __('Menu Header', 'yam'),
    'footer_menu' => __('Menu Footer', 'yam'),
));

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action('widgets_init', 'yam_widgets_init');

function yam_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar Principal', 'yam'),
        'id' => 'main_sidebar',
        'description' => __('Estos widgets seran vistos en las entradas y páginas del sitio', 'yam'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));

    register_sidebars(3, array(
        'name'          => __('Pie de Página %d', 'yam'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'yam'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ));

    if (class_exists('WooCommerce')) {
        register_sidebar(array(
            'name' => __('Sidebar de la Tienda', 'yam'),
            'id' => 'shop_sidebar',
            'description' => __('Estos widgets seran vistos en Tienda y Categorias de Producto', 'yam'),
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h2 class="widgettitle">',
            'after_title'   => '</h2>',
        ));
    }

    register_widget('yam_contact_widget');
}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_reports_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(9999, 400, true);
}
if (function_exists('add_image_size')) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('home_location', 570, 480, array('center', 'center'));
    add_image_size('archive_location_big', 480, 435, array('center', 'center'));
    add_image_size('archive_location_small', 225, 134, array('center', 'center'));
    add_image_size('boxed', 300, 300, array('center', 'center'));
    add_image_size('small', 200, 200, array('center', 'center'));
    add_image_size('main_image', 512, 355, array('center', 'center'));
}

/* --------------------------------------------------------------
    TRANSFORM VIDEO URL TO EMBED TYPE
-------------------------------------------------------------- */

function transform_embed_video($url)
{
    $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

    if (preg_match($longUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    $embed = 'https://www.youtube.com/embed/' . $youtube_id . '?rel=0&modestbranding=1';

    if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
        $id = $regs[3];
        $embed = "https://player.vimeo.com/video/" . $id;
    }

    return $embed;
}

/* --------------------------------------------------------------
    ADD CUSTOM LOCATION FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/class-locations-functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM SENDINBLUE FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/class-sendinblue-connector.php');

/* --------------------------------------------------------------
    AJAX SEND MESSAGE FROM FOOTER CONTACT
-------------------------------------------------------------- */
add_action('wp_ajax_custom_footer_send_message', 'custom_footer_send_message_handler');
add_action('wp_ajax_nopriv_custom_footer_send_message', 'custom_footer_send_message_handler');

function custom_footer_send_message_handler()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    $fullname = $_POST['formName'];
    $email = trim($_POST['formEmail']);
    $phone = trim($_POST['formPhone']);
    $comments = $_POST['formComments'];
    $grecaptcha = $_POST['g-recaptcha-response'];
    $list_id = $_POST['listID'];

    $fullname = explode(' ', $fullname);
    $fname = $fullname[0];
    $lname = $fullname[1];

    $data = array(
        'NOMBRE' => $fname,
        'APELLIDOS' => $lname,
        'SMS' => $phone,
        'COMMENTS' => $comments,
    );

    $sendinblue = new Sendinblue_Functions_Class;
    $arr_sendinblue = $sendinblue->create_contact($email, $list_id, $data);

    $google_options = get_option('yam_google_settings');

    if ($grecaptcha) {
        $post_data = http_build_query(
            array(
                'secret' => $google_options['secretkey'],
                'response' => $grecaptcha,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ),
            '',
            '&'
        );

        $opts = array(
            'http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $post_data
            )
        );

        $context  = stream_context_create($opts);
        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
        $captcha_response = json_decode($response);
    }

    if ($captcha_response->success == true) {
        ob_start();
        require_once get_theme_file_path('/templates/emails/contact-email.php');
        $logo = get_template_directory_uri() . '/images/logo.png';
        $body = ob_get_clean();
        $body = str_replace([
            '{fullname}',
            '{email}',
            '{phone}',
            '{message}',
            '{logo}'
        ], [
            $fullname,
            $email,
            $phone,
            $comments,
            $logo
        ], $body);

        require_once ABSPATH . WPINC . '/class-phpmailer.php';

        $mail = new PHPMailer;

        $mail->isHTML(true);
        $mail->Body = $body;
        $mail->CharSet = 'UTF-8';
        $mail->addAddress('ochoa.robert1@gmail.com');
        $mail->setFrom("noreply@{$_SERVER['SERVER_NAME']}", esc_html(get_bloginfo('name')));
        $mail->Subject = esc_html__('YamVacation: New Contact Message', 'yam');

        if (!$mail->send()) {
            wp_send_json_success(esc_html__("Thank you for submiting, We will be contacting you in the next 24 hours.", 'yam'), 200);
        } else {
            wp_send_json_success(esc_html__("Thank you for submiting, We will be contacting you in the next 24 hours.", 'yam'), 200);
        }
    } else {
        wp_send_json_error(esc_html__('Your message could not be sent. Please try again.', 'yam'), 400);
    }

    wp_die();
}

if (!function_exists('better_comments')) :
    function better_comments($comment, $args, $depth)
    {
?>
        <?php if ($comment->comment_approved != '0') : ?>
            <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
                <div class="comment">
                    <div class="comment-block">
                        <div class="comment-text">
                            <p> <?php comment_text() ?></p>
                        </div>
                        <div class="comment-stars">
                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                        <span class="comment-by">
                            <p><strong><?php echo get_comment_author() ?></strong></p>
                            <p><?php echo get_comment_meta(get_comment_ID(), 'city', true); ?></p>
                        </span>
                    </div>
                </div>
            </li>
        <?php endif; ?>
<?php
    }
endif;

function wpbeginner_comment_text_before($arg)
{
    $arg['comment_notes_before'] = "<p class='comment-title'>Leave a Review</p>";
    return $arg;
}

add_filter('comment_form_defaults', 'wpbeginner_comment_text_before');

function wpb_move_comment_field_to_bottom($fields)
{
    $comment_field = $fields['comment'];
    unset($fields['comment']);
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter('comment_form_fields', 'wpb_move_comment_field_to_bottom');


function wpbeginner_remove_comment_url($arg)
{
    $arg['url'] = '';
    return $arg;
}
add_filter('comment_form_default_fields', 'wpbeginner_remove_comment_url');

add_filter('comment_form_defaults', 'change_comment_form_defaults');
function change_comment_form_defaults($default)
{
    $commenter = wp_get_current_commenter();
    $default['fields']['email'] .= '<p class="comment-form-author">' . '<label for="city">' . __('City') . '</label> <span class="required">*</span> <input id="city" name="city" size="30" type="text" /></p>';
    return $default;
}

add_filter('preprocess_comment', 'verify_comment_meta_data');
function verify_comment_meta_data($commentdata)
{
    if (!isset($_POST['city']))
        wp_die(__('Error: please fill the required field (city).'));
    return $commentdata;
}

add_action('comment_post', 'save_comment_meta_data');
function save_comment_meta_data($comment_id)
{
    update_comment_meta($comment_id, 'city', $_POST['city']);
}

// Add shortcodes
require_once 'includes/class-shortcodes.php';
require_once 'includes/class-brochure-shortcode.php';
new Shortcodes();

/**
 * Check if the user has roles assigned
 * 
 * @param string|array $roles
 * @param int $user_id
 * @return bool
 */
function _has_role($roles, $user_id = null)
{
    if (null == $user_id) {
        if (!is_user_logged_in()) {
            return false;
        }

        $user = wp_get_current_user();
    } else {
        $user = new WP_User($user_id);
    }

    return (bool) array_intersect((array) $roles, $user->roles ?? []);
}

add_action('admin_init', 'my_remove_menu_pages');
function my_remove_menu_pages()
{
    global $menu;

    // Fix name
    foreach ($menu as &$item) {
        if (in_array('Accommodation', $item)) {
            foreach ($item as &$label) {
                if ($label == 'Accommodation') {
                    $label = 'Properties';
                    break;
                }
            }
            unset($label);
        }
        unset($item);
    }

    if (_has_role(['booking_administrator'])) { //your user id

        $remove_menus = [
            'options-general.php',
            'members',
            'tools.php',
            'edit.php',
        ];

        foreach ($remove_menus as $menu_slug) {
            remove_menu_page($menu_slug);
        }

        $remove_submenus = [
            'mphb_booking_menu' => [
                'mphb_calendar',
                'mphb_extensions',
            ]
        ];

        foreach ($remove_submenus as $menu_slug => $items) {
            foreach ($items as $submenu_slug) {
                remove_submenu_page($menu_slug, $submenu_slug);
            }
        }
    }
}
