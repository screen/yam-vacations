<?php

class Metaboxes_Booking
{
    public function __construct()
    {
        add_action('cmb2_admin_init', [$this, 'additional_data']);
        add_action('add_meta_boxes', [$this, 'generate_contract']);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function additional_data()
    {
        $cmb = new_cmb2_box([
            'id'           => 'mphb_booking_additional_data',
            'title'        => esc_html__('Additional', 'yam'),
            'object_types' => ['mphb_booking'],
        ]);

        $cmb->add_field([
            'name'             => 'Contract model',
            'desc'             => 'Select a contract model for this booking',
            'id'               => '_contract_id',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => $this->get_contract_options(),
        ]);

        $cmb->add_field([
            'name'             => 'Owner',
            'desc'             => 'Select an owner for the contract of this booking',
            'id'               => '_owner_id',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => $this->get_owner_options(),
        ]);

        $cmb->add_field([
            'name'    => 'Security deposit',
            'desc'    => 'Enter the amount of secure deposit without symbol or thousands separator',
            'default' => '0',
            'id'      => '_security_deposit',
            'type'    => 'text',
        ]);

        $cmb->add_field([
            'name'             => 'Broker',
            'desc'             => 'Optional. Select an owner for the contract of this booking',
            'id'               => '_broker_id',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => $this->get_broker_options(),
        ]);

        $cmb->add_field([
            'name'    => 'Broker amount',
            'desc'    => 'Required if is select a broker. Enter the brokerage amount without symbol or thousands separator',
            'default' => '0',
            'id'      => '_broker_amount',
            'type'    => 'text',
        ]);
    }

    /**
     * Get an associative array [ID => Label] of contracts
     *
     * @return array
     */
    private function get_contract_options()
    {
        $contracts = get_posts([
            'post_type'      => 'contract',
            'posts_per_page' => -1,
        ]);

        $result = [];

        foreach ($contracts as $contract) {
            $label = $contract->post_title;

            $terms = wp_get_post_terms($contract->ID, 'contract_tag');
            if (count($terms) > 0) {
                $term_labels = [];

                foreach ($terms as $term) {
                    $term_labels[] = $term->name;
                }

                $label = "{$label} (" . implode(', ', $term_labels) . ")";
            }

            $result[$contract->ID] = $label;
        }

        return $result;
    }

    /**
     * Get an associative array [ID => Label] of owners
     *
     * @return array
     */
    private function get_owner_options()
    {
        $owners = get_posts([
            'post_type'      => 'owner',
            'posts_per_page' => -1,
        ]);

        $result = [];

        foreach ($owners as $owner) {
            $result[$owner->ID] = $owner->post_title;
        }

        return $result;
    }

    /**
     * Get an associative array [ID => Label] of owners
     *
     * @return array
     */
    private function get_broker_options()
    {
        $brokers = get_posts([
            'post_type'      => 'broker',
            'posts_per_page' => -1,
        ]);

        $result = [];

        foreach ($brokers as $broker) {
            $result[$broker->ID] = $broker->post_title;
        }

        return $result;
    }

    public function generate_contract() {
        add_meta_box('generate_contract', __('Contract', 'yam'), [$this, 'generate_contract_callback'], 'mphb_booking', 'side');
    }

    /**
     * Undocumented function
     *
     * @param WP_Post $post
     * @return void
     */
    public function generate_contract_callback($post) {
        $download = admin_url("admin-ajax.php?action=download_customer_contract&booking_id={$post->ID}");
        $send     = admin_url("admin-ajax.php?action=send_customer_contract&booking_id={$post->ID}");
        ?>
        <a style="display: block;text-align: center; font-size: 1.25em; margin-bottom: 10px;" class="button-secondary" href="<?php echo $send ?>">Send contract to guest</a>
        <a style="display: block;text-align: center; font-size: 1.25em;" class="button-primary" href="<?php echo $download ?>">Download contract</a>
        <?php
    }
}



new Metaboxes_Booking();
