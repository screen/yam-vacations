<?php
// CHANGE DASHBOARD NAME FROM ACCOMODATIONS TO PROPERTIES
add_action('init', 'location_change_post_object', 999);

function location_change_post_object()
{
    $get_post_type = get_post_type_object('mphb_room_type');
    $labels = $get_post_type->labels;
    $labels->name = 'Properties';
    $labels->singular_name = 'Properties';
    $labels->add_new = 'Add Property';
    $labels->add_new_item = 'Add New Property';
    $labels->edit_item = 'Edit Property';
    $labels->new_item = 'Property';
    $labels->view_item = 'View Property';
    $labels->search_items = 'Search Property';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No Property found in Trash';
    $labels->all_items = 'All Properties';
    $labels->menu_name = 'Properties';
    $labels->name_admin_bar = 'Properties';
}

function get_location_restrictions($id)
{
    $arr_response = array();

    $currentDate = date('Y-m-d');
    
    $currentDate=date('Y-m-d', strtotime($currentDate));
    echo $currentDate . '<br/>';
    $general_restrictions = get_option('mphb_booking_rules_custom');

    foreach ($general_restrictions as $item) {
        if ($item['room_type_id'] == $id) {
            echo $item['date_from'] . '<br/>';
            echo $item['date_to'] . '<br/>';


            $contractDateBegin = date('Y-m-d', strtotime($item['date_from']));
            $contractDateEnd = date('Y-m-d', strtotime($item['date_to']));

            if (($currentDate >= $contractDateBegin) && ($currentDate <= $contractDateEnd)) {
                /*
                $arr_response = array(
                    '' => ''
                );
                */
            }
        }
    }
}

function get_location_rates_price($id)
{
    $arr_rates = new WP_Query(array('post_type' => 'mphb_rate', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date', 'meta_query' => array(
    array(
        'key'     => 'mphb_room_type_id',
        'value'   => $id,
        'compare' => '=',
    )
    )));

    if ($arr_rates->have_posts()) :
    while ($arr_rates->have_posts()) : $arr_rates->the_post();
    $rate_id = get_the_ID();
    $rates = get_post_meta($rate_id, 'mphb_season_prices', true);
    $season_element = $rates[0]['price'];

    $periods = $season_element['periods'];
    $prices = $season_element['prices'];

    endwhile;
    endif;
    wp_reset_query();

    $price_periods = array();

    if (!empty($periods)) {
        foreach ($periods as $key => $value) {
            $price_periods[$value] = $prices[$key];
        }
    }

    return $price_periods;
}


add_action('wp_ajax_check_location_reservation', 'check_location_reservation_handler');
add_action('wp_ajax_nopriv_check_location_reservation', 'check_location_reservation_handler');

function check_location_reservation_handler()
{
    $arr_dates = array();
    $isTaken = false;

    // OBTENGO ID Y RANGO DE FECHAS
    $location_id = $_POST['locationID'];
    $checkIn = $_POST['checkInDate'];
    $checkOut = $_POST['checkOutDate'];
    $diff = $_POST['diffDays'];

    // TRANSFORMAR LA FECHA
    $time1 = strtotime($checkIn);
    $newformat1 = date('Y-m-d', $time1);

    // OBTENGO LOS BOOKINGS
    $arr_bookings = new WP_Query(array('post_type' => 'mphb_booking', 'posts_per_page' => -1, 'post_status' => 'confirmed', 'meta_query' => array(
        array(
            'key'     => 'yam_location_id',
            'value'   => $location_id ,
            'compare' => '=',
        ),
    )));

    if ($arr_bookings->have_posts()) :
    while ($arr_bookings->have_posts()) : $arr_bookings->the_post();
    $beginDate = get_post_meta(get_the_ID(), 'mphb_check_in_date', true);
    $endDate = get_post_meta(get_the_ID(), 'mphb_check_out_date', true);
    $contractDateBegin = date('Y-m-d', strtotime($beginDate));
    $contractDateEnd = date('Y-m-d', strtotime($endDate));

    array_push($arr_dates, $beginDate . ',' . $endDate);
    for ($i = 0; $i < $diff; $i++) {
        // LE AGREGO UN DIA POR CADA DIA
        $plusstring = '+' . $i . ' day';
        $dayplus = date('Y-m-d', strtotime($newformat1 . $plusstring));
        // COMPARO LAS FECHAS
        if (($dayplus >= $contractDateBegin) && ($dayplus <= $contractDateEnd)) {
            $isTaken = true;
        }
    }
    endwhile;
    endif;
    wp_reset_query();

    // DEVUELVO DATA
    $data = array(
        'taken' => $isTaken,
        'dates' => $arr_dates
    );
    echo json_encode($data);
    wp_die();
}

add_action('wp_ajax_make_complete_reservation', 'make_complete_reservation_handler');
add_action('wp_ajax_nopriv_make_complete_reservation', 'make_complete_reservation_handler');

function make_complete_reservation_handler()
{
    parse_str($_POST['info'], $info);

    $id_unique = uniqid();

    $booking_key = 'booking_' . $id_booking . '_' . $id_unique;

    $time1 = strtotime($info['date-in']);
    $newformat1 = date('Y-m-d', $time1);

    $time2 = strtotime($info['date-out']);
    $newformat2 = date('Y-m-d', $time2);

    $post_title = $info['propertyID'] . '_' . $id_unique;

    $room_type = get_post($info['propertyID']);

    $subtotal = $info['priceTotal'] - 900;

    $list_days = array();

    for ($i = 0; $i < $info['daysQty']; $i++) {
        $plusstring = '+' . $i . ' day';
        $dayplus = date('Y-m-d', strtotime($newformat1 . $plusstring));
        $list_days[$dayplus] = $info['priceSingle'];
    }

    $jayParsedAry = [
        "rooms" => [
              [
                 "room" => [
                    "type" => $room_type->post_title,
                    "rate" => $room_type->post_title,
                    "list" => $list_days,
                    "total" => $subtotal,
                    "discount" => "0",
                    "discount_total" => $subtotal,
                    "adults" => $info['guest-adults'],
                    "children" => $info['guest-youth'],
                    "children_capacity" => $info['guest-infant']
                 ],
                 "services" => [
                     "list" => [],
                     "total" => "0"
                 ],
                 "fees" => [
                    "list" => [],
                    "total" => "0"
                ],
                "taxes" => [
                    "room" => [
                        "list" => [],
                        "total" => "0"
                    ],
                    "services" => [
                        "list" => [
                            [
                                "label" => "Cleaning Fee",
                                "price" => "600"
                            ],
                            [
                                "label" => "Service Fee",
                                "price" => "0"
                            ],
                            [
                                "label" => "Ocupancy taxes and fees",
                                "price" => "300"
                            ]
                        ],
                        "total" => "900"
                    ],
                    "fees" => [
                        "list" => [],
                        "total" => "0"
                        ]
                ],
                "total" => $subtotal,
                "discount_total" => $subtotal
              ]
           ],
        "total" => $subtotal
     ];

    $my_post = array(
        'post_type'     => 'mphb_booking',
        'post_title'    => wp_strip_all_tags($post_title),
        'post_status'   => 'pending',
        'post_author'   => 1,
        'meta_input' => array(
            'yam_location_id' => $info['propertyID'],
            'yam_location_adults' => $info['guest-adults'],
            'yam_location_youth' => $info['guest-youth'],
            'yam_location_infant' => $info['guest-infant'],
            'mphb_key' => $booking_key,
            'mphb_check_in_date' => $newformat1,
            'mphb_check_out_date' => $newformat2,
            'mphb_note' => $info['comments'],
            'mphb_email' => $info['EmailAddress'],
            'mphb_first_name' => $info['firstName'],
            'mphb_last_name' => $info['lastName'],
            'mphb_phone' => $info['phone'],
            'mphb_country' => $info['location'],
            'mphb_address1' => $info['location'],
            'mphb_total_price' => $info['priceTotal'],
            '_mphb_checkout_id' => $id_unique,
            'mphb_language' => 'en',
            '_mphb_booking_price_breakdown' => json_encode($jayParsedAry)
        )
      );
    $id_booking = wp_insert_post($my_post);

    yam_send_admin_booking_email($id_booking);
    
    wp_die();
}

function yam_send_admin_booking_email($id_booking)
{
    $location_id = get_post_meta($id_booking, 'yam_location_id', true);
    $checkIn = get_post_meta($id_booking, 'mphb_check_in_date', true);
    $checkOut = get_post_meta($id_booking, 'mphb_check_out_date', true);
    $firstName = get_post_meta($id_booking, 'mphb_first_name', true);
    $lastName = get_post_meta($id_booking, 'mphb_last_name', true);
    $email = get_post_meta($id_booking, 'mphb_email', true);
    $phone = get_post_meta($id_booking, 'mphb_phone', true);
    $comments = get_post_meta($id_booking, 'mphb_note', true);
    $totalPrice = get_post_meta($id_booking, 'mphb_total_price', true);

    $current_location = get_post($location_id);

    $location_link = get_permalink($current_location);
    
    $content = get_option('mphb_email_admin_pending_booking_content');
    $content = str_replace('%booking_id%', $id_booking, $content);
    $content = str_replace('%check_in_date%', $checkIn, $content);
    $content = str_replace('%check_out_date%', $checkOut, $content);
    $content = str_replace('%reserved_rooms_details%', '<a href="' . $location_link . '" target="_blank">' . $current_location->post_title . '</a>', $content);
    $content = str_replace('%customer_first_name%', $firstName, $content);
    $content = str_replace('%customer_last_name%', $lastName, $content);
    $content = str_replace('%customer_email%', '<a href="mailto:' . $email . '" target="_blank">' . $email . '</a>', $content);
    $content = str_replace('%customer_phone%', '<a href="tel:' . $phone . '" target="_blank">' . $phone . '</a>', $content);
    $content = str_replace('%customer_note%', $comments, $content);
    $content = str_replace('%booking_total_price%', '$ ' . number_format($totalPrice, 2), $content);
    
    $logo = get_template_directory_uri() . '/images/logo.png';

    ob_start();
    require_once get_theme_file_path('/templates/admin-new-booking.php');
    $body = ob_get_clean();
    $body = str_replace([
        '{contenido}',
        '{logo}'
        
    ], [
        $content,
        $logo
        
    ], $body);

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML(true);
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress('ochoa.robert1@gmail.com');
    $mail->setFrom("noreply@{$_SERVER['SERVER_NAME']}", esc_html(get_bloginfo('name')));
    $mail->Subject = esc_html__('YamVacations: New Booking Submission', 'yam');
    $mail->send();

    /* USER NOTIFICATION */
    $content = get_option('mphb_email_customer_pending_booking_content');
    $content = str_replace('%booking_id%', $id_booking, $content);
    $content = str_replace('%check_in_date%', $checkIn, $content);
    $content = str_replace('%check_out_date%', $checkOut, $content);
    $content = str_replace('%reserved_rooms_details%', '<a href="' . $location_link . '" target="_blank">' . $current_location->post_title . '</a>', $content);
    $content = str_replace('%customer_first_name%', $firstName, $content);
    $content = str_replace('%customer_last_name%', $lastName, $content);
    $content = str_replace('%customer_email%', '<a href="mailto:' . $email . '" target="_blank">' . $email . '</a>', $content);
    $content = str_replace('%customer_phone%', '<a href="tel:' . $phone . '" target="_blank">' . $phone . '</a>', $content);
    $content = str_replace('%customer_note%', $comments, $content);
    $content = str_replace('%booking_total_price%', '$ ' . number_format($totalPrice, 2), $content);
    
    $logo = get_template_directory_uri() . '/images/logo.png';

    ob_start();
    require_once get_theme_file_path('/templates/user-new-booking.php');
    $body = ob_get_clean();
    $body = str_replace([
        '{contenido}',
        '{logo}'
        
    ], [
        $content,
        $logo
        
    ], $body);

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML(true);
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress($email);
    $mail->setFrom("noreply@{$_SERVER['SERVER_NAME']}", esc_html(get_bloginfo('name')));
    $mail->Subject = esc_html__('YamVacations: Your Booking Submission', 'yam');

    if (!$mail->send()) {
        wp_send_json_success($mail, 200);
    } else {
        wp_send_json_success($mail, 200);
    }
}
