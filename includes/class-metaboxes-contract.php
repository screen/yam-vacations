<?php

class Metaboxes_Contract
{
    public function __construct()
    {
        add_action('add_meta_boxes', [$this, 'contract_placholder_info']);
    }


    public function contract_placholder_info()
    {
        add_meta_box('contract_placholder_info', __('Placeholders', 'yam'), [$this, 'contract_placholder_info_callback'], 'contract', 'side');
    }

    /**
     * Undocumented function
     *
     * @param WP_Post $post
     * @return void
     */
    public function contract_placholder_info_callback($post)
    {
?>
        <pre>{payment-table}
{signature-table}
{guest-name}
{owner-name}
{broker-name}
{broker-agent}
{broker-amount}
{property-location}
{property-amenities}
{arrival-date}
{departure-date}
{max-occupation}
{security-deposit}
{rental-price}
{total-amount-due}
{cleaning-fee}
{wire-instructions}</pre>
<?php
    }
}



new Metaboxes_Contract();
