<?php

use Dompdf\Dompdf;
use Dompdf\Options;

add_action('wp_ajax_create_report_by_location', 'create_report_by_location_handler');
add_action('wp_ajax_nopriv_create_report_by_location', 'create_report_by_location_handler');

function create_report_by_location_handler()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    $id_location = $_REQUEST['id'];
    $contact_info = $_REQUEST['contact-info'] ?? '';

    if (!empty($contact_info)) {
        $contact_info = sanitize_textarea_field($contact_info);
    }

    if (!empty($contact_info)) {
        $contact_info = '<h2>Contact Info</h2>' . apply_filters('the_content', $contact_info);
        $contact_info = '<div class="contact-info">' . $contact_info . '</div>';
    }

    if ($id_location == 'all') {
        $locations = get_posts([
            'post_type' => 'mphb_room_type',
            'posts_per_page' => -1,
        ]);
    } elseif (intval($id_location) > 0) {
        $locations = get_posts([
            'post__in' => [intval($id_location)],
            'post_type' => 'mphb_room_type',
            'posts_per_page' => 1,
        ]);
    } else {
        wp_die('Invalid request');
    }

    global $post;

    $properties = [];

    foreach ($locations as $post) {
        setup_postdata($post);

        // CATEGORY
        $item_data = get_the_terms(get_the_ID(), 'mphb_room_type_category');
        foreach ($item_data as $item) {
            $arr_element[] = $item->name;
        }
        $property_category = join(', ', $arr_element);

        // LOCATION
        $item_data = get_the_terms(get_the_ID(), 'mphb_ra_location-address');
        foreach ($item_data as $item) {
            $arr_locations[] = $item->name;
        }
        $property_location = join(', ', $arr_locations);

        // NEAR BY
        $item_data = get_the_terms(get_the_ID(), 'mphb_ra_points-of-interest-nearb');
        foreach ($item_data as $item) {
            $arr_nearby[] = $item->name;
        }
        $property_nearby = join(', ', $arr_nearby);

        $property_guests         = get_post_meta(get_the_ID(), 'yam_location_guests', true);
        $property_bedrooms       = get_post_meta(get_the_ID(), 'yam_location_bedrooms', true);
        $property_bathrooms      = get_post_meta(get_the_ID(), 'yam_location_bathrooms', true);
        $property_half_bathrooms = get_post_meta(get_the_ID(), 'yam_location_half_bathrooms', true);
        $property_minimum        = get_post_meta(get_the_ID(), 'yam_location_minimum_stay', true);
        $property_maximum        = get_post_meta(get_the_ID(), 'yam_location_maximum_stay', true);
        $property_special        = get_post_meta(get_the_ID(), 'yam_location_special', true);
        $property_additional_rules = apply_filters('the_content', get_post_meta(get_the_ID(), 'yam_additional_rules', true));

        ob_start();

        get_template_part('templates/brochure/pdf/loop');

        $property = ob_get_clean();
        $property = str_replace([
            '{contact-info}',
            '{property_location}',
            '{property_category}',
            '{property_guests}',
            '{property_bedrooms}',
            '{property_bathrooms}',
            '{property_half_bathrooms}',
            '{property_minimum}',
            '{property_maximum}',
            '{property_special}',
            '{property_additional_rules}',
            '{property_nearby}'
        ], [
            $contact_info,
            $property_location,
            $property_category,
            $property_guests,
            $property_bedrooms,
            $property_bathrooms,
            $property_half_bathrooms,
            $property_minimum,
            $property_maximum,
            $property_special,
            $property_additional_rules,
            $property_nearby
        ], $property);

        $properties[] = $property;
    }

    ob_start();
    get_template_part('templates/brochure/pdf/container');
    $container = ob_get_clean();

    $html = str_replace('{content}', implode('<div class="page-break"></div>', $properties), $container);

    require_once('vendor/autoload.php');

    $options = new Options();
    $options->set('isRemoteEnabled', true);
    $options->set('isHtml5ParserEnabled', true);
    $options->set('isFontSubsettingEnabled', true);
    $options->set('defaultMediaType', 'all');

    $dompdf = new Dompdf($options);
    $dompdf->setPaper("Letter", "portrait");
    $dompdf->setOptions($options);

    $pdf_name = 'all-properties.pdf';

    if (count($locations) == 1) {
        $pdf_name = sanitize_title($locations[0]->post_title);
    }

    // Cargamos el contenido HTML.
    $dompdf->loadHtml($html);
    // Renderizamos el documento PDF.
    $dompdf->render();
    $pdf = $dompdf->output(['isRemoteEnabled' => true]);
    $dompdf->stream($pdf_name, ['Attachment' => 1]);
    exit(0);
}

add_action('wp_ajax_download_customer_contract', ['Generate_Contract', 'download']);
add_action('wp_ajax_send_customer_contract', ['Generate_Contract', 'send_to_email']);

class Generate_Contract
{

    /**
     *
     * @var array
     */
    protected static $wp_mail_headers = [
        'Content-Type: text/html; charset=UTF-8',
        //'Bcc: jprieto@screen.com.ve'
    ];

    /**
     * Undocumented function
     *
     * @return WP_Post
     */
    private static function get_booking()
    {
        $booking_id = (int) sanitize_text_field($_REQUEST['booking_id']);
        if (!$booking_id) {
            wp_die('BOOKING_ERROR_001: Invalid request');
        }

        $booking = get_post($booking_id);
        if (empty($booking) || $booking->post_type != 'mphb_booking') {
            wp_die('BOOKING_ERROR:002: Invalid request');
        }

        return $booking;
    }

    /**
     * Undocumented function
     *
     * @param WP_Post $booking
     * @return WP_Post
     */
    private static function get_owner($booking = null)
    {
        if (empty($booking)) {
            $booking = self::get_booking();
        }

        $owner_id = (int) get_post_meta($booking->ID, '_owner_id', true);
        if (!$owner_id) {
            wp_die('OWNER_ERROR_001: Invalid owner');
        }

        $owner = get_post($owner_id);
        if (empty($owner) || $owner->post_type != 'owner') {
            wp_die('OWNER_ERROR_002: Invalid owner');
        }

        return $owner;
    }

    /**
     * Undocumented function
     *
     * @param WP_Post $booking
     * @return WP_Post
     */
    private static function get_contract($booking = null)
    {
        if (empty($booking)) {
            $booking = self::get_booking();
        }

        $contract_id = (int) get_post_meta($booking->ID, '_contract_id', true);
        if (!$contract_id) {
            wp_die('CONTRACT_ERROR_001: Invalid contract');
        }

        $contract = get_post($contract_id);
        if (empty($contract) || $contract->post_type != 'contract') {
            wp_die('CONTRACT_ERROR_002: Invalid contract');
        }

        return $contract;
    }


    /**
     * Undocumented function
     *
     * @return Dompdf
     */
    public static function generate()
    {
        if (defined('WP_DEBUG') && WP_DEBUG) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }

        $booking  = self::get_booking();
        $contract = self::get_contract($booking);
        $owner    = self::get_owner($booking);
        // Get owner

        // Get broker
        $broker_id = (int) get_post_meta($booking->ID, '_broker_id', true);
        if ($broker_id > 0) {
            $broker = get_post($broker_id);
            if (empty($broker) || $broker->post_type != 'broker') {
                wp_die('BROKER_ERROR_002: Invalid broker');
            }

            $broker_agent = get_post_meta($broker->ID, '_agent_name', true);
            $broker_amount = get_post_meta($booking->ID, '_broker_amount', true);
        } else {
            $broker = null;
            $broker_agent = null;
            $broker_amount = null;
        }

        // Booking info
        $guest_name      = get_post_meta($booking->ID, 'mphb_first_name', true) . ' ' . get_post_meta($booking->ID, 'mphb_last_name', true);
        $price_breakdown = get_post_meta($booking->ID, '_mphb_booking_price_breakdown', true);
        $cleaning_fee    = 0;

        try {
            $price_breakdown = json_decode($price_breakdown);
            $list            = $price_breakdown->rooms[0]->taxes->services->list;
            foreach ($list as $item) {
                if (strtolower($item->label) == 'cleaning fee') {
                    $cleaning_fee = $item->price;
                    break;
                }
            }
        } catch (\Throwable $th) {
            wp_die('INCOMPLETE_INFO_001: Invalid booking');
        }

        $arrival_date = get_post_meta($booking->ID, 'mphb_check_in_date', true);
        $date         = DateTime::createFromFormat('Y-m-d', $arrival_date);
        $arrival_date = $date->format('F d, Y');

        $departure_date = get_post_meta($booking->ID, 'mphb_check_out_date', true);
        $date           = DateTime::createFromFormat('Y-m-d', $departure_date);
        $departure_date = $date->format('F d, Y');

        $rental_price     = (float) get_post_meta($booking->ID, 'mphb_total_price', true);
        $security_deposit = (float) get_post_meta($booking->ID, '_security_deposit', true);

        // Property info
        $property_id = get_post_meta($booking->ID, 'yam_location_id', true);

        $property = get_post($property_id);
        if (empty($property) || $property->post_type != 'mphb_room_type') {
            wp_die('PROPERTY_ERROR_002: Invalid request');
        }

        $max_occupation = (int) get_post_meta($property->ID, 'mphb_total_capacity', true);

        $amenities = wp_get_object_terms($property_id, 'mphb_room_type_facility');
        $property_amenities = '';
        if (!is_wp_error($amenities) && count($amenities) > 0) {
            $property_amenities .= '<ul>';
            foreach ($amenities as $amenity) {
                $property_amenities .= "<li>{$amenity->name}</li>";
            }
            $property_amenities .= '</ul>';
        }

        // Main contract content
        ob_start();
        get_template_part('templates/booking/contract', null, compact('contract'));
        $content = ob_get_clean();

        // Payment snippet
        ob_start();
        get_template_part('templates/booking/payment-table', null, compact('security_deposit'));
        $payment_table = ob_get_clean();

        // Signature snippet
        ob_start();
        get_template_part('templates/booking/signature-table', null, compact('booking'));
        $signature_table = ob_get_clean();

        $number_text = new NumberFormatter("en_US", NumberFormatter::SPELLOUT);
        $number_currency = new NumberFormatter("en_US", NumberFormatter::CURRENCY);

        // Fill placeholders
        $replaces = [
            '{payment-table}'      => $payment_table,
            '{signature-table}'    => $signature_table,
            '{guest-name}'         => $guest_name,
            '{owner-name}'         => $owner->post_title,
            '{broker-name}'        => $broker->post_title ?? '{broker-name}',
            '{broker-agent}'       => $broker_agent ?? '{broker-agent}',
            '{broker-amount}'      => $broker_amount ?? '{broker-amount}',
            '{property-location}'  => $property->post_title,
            '{property-amenities}' => $property_amenities,
            '{arrival-date}'       => $arrival_date,
            '{departure-date}'     => $departure_date,
            '{max-occupation}'     => "{$number_text->format($max_occupation)} ({$max_occupation})",
            '{security-deposit}'   => $number_currency->format($security_deposit),
            '{rental-price}'       => $number_currency->format($rental_price),
            '{total-amount-due}'   => $number_currency->format($rental_price + $security_deposit),
            '{cleaning-fee}'       => $number_currency->format($cleaning_fee),
            '{wire-instructions}'  => '<div class="page-break"></div>' . apply_filters('the_content', $owner->post_content),
        ];

        $contract->post_content = str_replace(array_keys($replaces), array_values($replaces), $contract->post_content);

        // Replace placeholders
        $html = str_replace('{content}', $contract->post_content, $content);

        // die($html);

        require_once('vendor/autoload.php');

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isFontSubsettingEnabled', true);
        $options->set('defaultMediaType', 'all');
        $options->set('isPhpEnabled', true);

        $dompdf = new Dompdf($options);
        $dompdf->setPaper("Letter", "portrait");
        $dompdf->setOptions($options);

        // Cargamos el contenido HTML.
        $dompdf->loadHtml($html);
        // Renderizamos el documento PDF.
        $dompdf->render();

        return $dompdf;
    }

    /**
     * Generate a contract and download
     *
     * @return void
     */
    public static function download()
    {
        $dompdf = self::generate();
        $dompdf->output(['isRemoteEnabled' => true]);
        $dompdf->stream('contract.pdf', ['Attachment' => 1]);
        exit(0);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function send_to_email()
    {
        if (defined('WP_DEBUG') && WP_DEBUG) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }

        $booking = self::get_booking();
        $email = get_post_meta($booking->ID, 'mphb_email', true);

        if (!is_email($email)) {
            wp_die('SEND_CONTRACT_001: Invalid email');
        }

        try {
            add_action('phpmailer_init', [__CLASS__, 'add_attachment']);
            wp_mail($email, __('Your contract', 'yam'), __('Hi! This is your car contract.', 'ontrac'), self::$wp_mail_headers);
        } catch (Exception $exc) {
            wp_die('SEND_CONTRACT_002: Error on sending email');
        }

        wp_safe_redirect(admin_url("post.php?post={$booking->ID}&action=edit"));
        exit(0);
    }

    /**
     *
     * @param type $phpmailer
     */
    public static function add_attachment($phpmailer)
    {
        $dompdf     = self::generate();
        $pdf_string = $dompdf->output(['isRemoteEnabled' => true]);
        $phpmailer->addStringAttachment($pdf_string, 'booking.pdf');
    }
}
