<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Yam_Concierge_Metaboxes')) :
    class Yam_Concierge_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'yam_concierge_custom_metabox'));
        }

        public function yam_concierge_custom_metabox()
        {
            /* 1.- HOME: HERO SECTION */
            $cmb_concierge_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'concierge_metabox',
                'title'         => esc_html__('Concierge Services', 'yam'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-concierges.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $group_field_id = $cmb_concierge_metabox->add_field(array(
                'id'            => parent::PREFIX . 'concierge_group',
                'name'          => esc_html__('Services Group', 'yam'),
                'description'   => __('Group of Services inside this section', 'yam'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Service {#}', 'yam'),
                    'add_button'        => __('Add other Service', 'yam'),
                    'remove_button'     => __('Remove Service', 'yam'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this service?', 'yam')
                )
            ));

            $cmb_concierge_metabox->add_group_field($group_field_id, array(
                'id'        => 'image',
                'name'      => esc_html__('Image', 'yam'),
                'desc'      => esc_html__('Upload an image for this service', 'yam'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload image', 'yam'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_concierge_metabox->add_group_field($group_field_id, array(
                'id'        => 'title',
                'name'      => esc_html__('Service Title', 'yam'),
                'desc'      => esc_html__('Insert a descriptive title for this service', 'yam'),
                'type'      => 'text'
            ));

            $cmb_concierge_metabox->add_group_field($group_field_id, array(
                'id'        => 'content',
                'name'      => esc_html__('Link Title', 'yam'),
                'desc'      => esc_html__('Insert a descriptive title for this link', 'yam'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_concierge_metabox->add_group_field($group_field_id, array(
                'id'        => 'modal',
                'name'      => esc_html__('Activate Request Modal', 'yam'),
                'desc'      => esc_html__('Activate only if this button must open a modal', 'yam'),
                'type'      => 'checkbox'
            ));

            $cmb_concierge_metabox->add_group_field($group_field_id, array(
                'id'        => 'link_url',
                'name'      => esc_html__('Service Button Link URL', 'yam'),
                'desc'      => esc_html__('Insert a link url title for this buttton', 'yam'),
                'type'      => 'text_url'
            ));

            $cmb_concierge_metabox->add_group_field($group_field_id, array(
                'id'        => 'link_text',
                'name'      => esc_html__('Service Button Text', 'yam'),
                'desc'      => esc_html__('Insert a descriptive text for this button', 'yam'),
                'type'      => 'text'
            ));
        }
    }
endif;

new Yam_Concierge_Metaboxes;
