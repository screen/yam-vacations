<?php

/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/**
 * Sendinblue Custom Functions
 */

if (!class_exists('Sendinblue_Functions_Class')) :

    class Sendinblue_Functions_Class
    {
        protected $apikey = '';
        
        /**
         * Main Sub-Constructor.
         */
        public function __construct()
        {
            $sendinblue_settings = get_option('yam_sendinblue_settings');
            $this->apikey = $sendinblue_settings['apikey'];
        }

        /**
         * cUrl Main Handler
         */
        public function curl_constructor($url, $method, $content)
        {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', 'api-key:' . $this->apikey ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_ENCODING, '');
            curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, 'CURL_HTTP_VERSION_1_1');
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($content));
            }
            if ($method == 'GET') {
                curl_setopt($curl, CURLOPT_HTTPGET, 1);
            }
            if ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($content));
            }
            $respuesta = json_decode(curl_exec($curl));
            $err = curl_error($curl);
            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if ($http_code == 400) {
                if (isset($respuesta->code)) {
                    $codigo = $respuesta->code;
                    switch ($codigo) {
                        case 'duplicate_parameter':
                            if ($respuesta->message == 'Unable to create contact, SMS is already associate with another Contact') {
                                $http_code = 400;
                            } else {
                                $http_code = 400;
                            }
                            break;
                            
                        default:
                            $http_code = 500;
                            break;
                    }
                }
            }
            
            $arr_data = array(
                'respuesta' => $respuesta,
                'http_code' => $http_code
            );

            return $arr_data;
        }

        /**
         * Create a contact
         */
        public function create_contact($email, $list_id, $data)
        {
            $createContact = array();
            $createContact['email'] = $email;
            $createContact['attributes'] = $data;
            $createContact['listIds'] = array(intval($list_id));
            $createContact['emailBlacklisted'] = false;
            $createContact['smsBlacklisted'] = false;
            $createContact['updateEnabled'] = false;

            $url = 'https://api.sendinblue.com/v3/contacts';
            $method = 'POST';

            $respuesta = $this->curl_constructor($url, $method, $createContact);

            
            if (isset($respuesta['http_code'])) {
                if ($respuesta['http_code'] == 400) {
                    $respuesta = $this->update_contact($email, $list_id, $data);
                }
            }

            return $respuesta;
        }

        /**
         * Update a contact
         */
        public function update_contact($email, $list_id, $data)
        {
            foreach($data as $key => $value) {
                if ($key != 'SMS') {
                    $new_data[$key] = $value;
                }
            }
            $createContact = array();
            $createContact['email'] = $email;
            $createContact['attributes'] = $new_data;
            $createContact['listIds'] = array(intval($list_id));
            $createContact['emailBlacklisted'] = false;
            $createContact['smsBlacklisted'] = false;
            $createContact['updateEnabled'] = true;

            $url = 'https://api.sendinblue.com/v3/contacts/' . urlencode($email);
            $method = 'PUT';

            $respuesta = $this->curl_constructor($url, $method, $createContact);
            return $respuesta;
        }
    }

    /* USE PROCEDURE */
    /*
        $list_id = $submit['list_id'];
        $email = $submit['contact_email'];
        $data = array(
            'FIRSTNAME' => $submit['contact_name'],
            'SMS' => $submit['contact_telf'],
            'BEST_TIME_TO_CALL' => $submit['contact_best_time'],
            'COMMENTS' => $submit['contact_message'],
        );

        $sendinblue = new Sendinblue_Functions_Class;
        $arr_sendinblue = $sendinblue->create_contact($email, $list_id, $data);
    */
    
endif;