<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}
/**
 * CMB2 Custom Metaboxes
 *
 * @link https://woocommerce.com/
 *
 * @package yam
 */
class customCMB2Class
{
    const PREFIX = 'yam_';
    /**
     * Main Constructor.
     */
    public function __construct()
    {
        add_filter('cmb2_show_on', array($this, 'ed_metabox_include_front_page'), 10, 2);
        add_filter('cmb2_show_on', array($this, 'be_metabox_show_on_slug'), 10, 2);
    }

    /**
     * Include custom 'show_on' for Front Page
     */
    public function ed_metabox_include_front_page($display, $meta_box)
    {
        if (!isset($meta_box['show_on']['key'])) {
            return $display;
        }

        if ('front-page' !== $meta_box['show_on']['key']) {
            return $display;
        }

        $post_id = 0;

        // If we're showing it based on ID, get the current ID
        if (isset($_GET['post'])) {
            $post_id = $_GET['post'];
        } elseif (isset($_POST['post_ID'])) {
            $post_id = $_POST['post_ID'];
        }

        if (!$post_id) {
            return false;
        }

        // Get ID of page set as front page, 0 if there isn't one
        $front_page = get_option('page_on_front');

        // there is a front page set and we're on it!
        return $post_id == $front_page;
    }

    /**
     * Include custom 'show_on' for Page Slug
     */
    public function be_metabox_show_on_slug($display, $meta_box)
    {
        if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
            return $display;
        }

        if ('slug' !== $meta_box['show_on']['key']) {
            return $display;
        }

        $post_id = 0;

        // If we're showing it based on ID, get the current ID
        if (isset($_GET['post'])) {
            $post_id = $_GET['post'];
        } elseif (isset($_POST['post_ID'])) {
            $post_id = $_POST['post_ID'];
        }

        if (!$post_id) {
            return $display;
        }

        $slug = get_post($post_id)->post_name;

        // See if there's a match
        return in_array($slug, (array) $meta_box['show_on']['value']);
    }
}

// Initialize class
new customCMB2Class;

/* GETTING REQUIRED FILES */
require_once('class-metaboxes-home.php');
require_once('class-metaboxes-agents.php');
require_once('class-metaboxes-concierge.php');
require_once('class-metaboxes-yatchs.php');
require_once('class-metaboxes-booking.php');
require_once('class-metaboxes-broker.php');
require_once('class-metaboxes-contract.php');

/* GET LOCATIONS METABOXES */
require_once('class-metaboxes-locations.php');

/**
 * Register meta box(es).
 */
function yam_custom_register_meta_boxes()
{
    add_meta_box('custom_booking_box_id', __('Booking Information', 'yam'), 'yam_booking_metabox_callback', 'mphb_booking');
}
add_action('add_meta_boxes', 'yam_custom_register_meta_boxes');
 

function yam_booking_metabox_callback($post)
{
    ?>
<div class="inside">
    <table class="form-table">
        <tr>
            <th>Location:</th>
            <?php $post_id = get_post_meta($post->ID, 'yam_location_id', true); ?>
            <?php $location = get_post($post_id); ?>
            <td><?php echo $location->post_title; ?></td>
        </tr>
        <tr>
            <th>Adults:</th>
            <?php $adults = get_post_meta($post->ID, 'yam_location_adults', true); ?>
            <td><?php echo $adults; ?></td>
        </tr>
        <tr>
            <th>Adults (Ages 2-12):</th>
            <?php $adults = get_post_meta($post->ID, 'yam_location_youth', true); ?>
            <td><?php echo $adults; ?></td>
        </tr>
        <tr>
            <th>Infants (Under 2):</th>
            <?php $adults = get_post_meta($post->ID, 'yam_location_infant', true); ?>
            <td><?php echo $adults; ?></td>
        </tr>

    </table>
</div>
<?php
}
 
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
/*
function wpdocs_save_meta_box($post_id)
{
    // Save logic goes here. Don't forget to include nonce checks!
}
add_action('save_post', 'wpdocs_save_meta_box');
*/