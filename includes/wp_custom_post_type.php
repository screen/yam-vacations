<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

// Register Custom Post Type
function yatch_custom_post_type()
{

	$labels = array(
		'name'                  => _x('Yatchs', 'Post Type General Name', 'yam'),
		'singular_name'         => _x('Yatch', 'Post Type Singular Name', 'yam'),
		'menu_name'             => __('Yatchs', 'yam'),
		'name_admin_bar'        => __('Yatchs', 'yam'),
		'archives'              => __('Yatch Archives', 'yam'),
		'attributes'            => __('Yatch Attributes', 'yam'),
		'parent_item_colon'     => __('Parent Yatch:', 'yam'),
		'all_items'             => __('All Yatchs', 'yam'),
		'add_new_item'          => __('Add New Yatch', 'yam'),
		'add_new'               => __('Add New', 'yam'),
		'new_item'              => __('New Yatch', 'yam'),
		'edit_item'             => __('Edit Yatch', 'yam'),
		'update_item'           => __('Update Yatch', 'yam'),
		'view_item'             => __('View Yatch', 'yam'),
		'view_items'            => __('View Yatchs', 'yam'),
		'search_items'          => __('Search Yatch', 'yam'),
		'not_found'             => __('Not found', 'yam'),
		'not_found_in_trash'    => __('Not found in Trash', 'yam'),
		'featured_image'        => __('Featured Image', 'yam'),
		'set_featured_image'    => __('Set featured image', 'yam'),
		'remove_featured_image' => __('Remove featured image', 'yam'),
		'use_featured_image'    => __('Use as featured image', 'yam'),
		'insert_into_item'      => __('Insert into Yatch', 'yam'),
		'uploaded_to_this_item' => __('Uploaded to this Yatch', 'yam'),
		'items_list'            => __('Yatchs list', 'yam'),
		'items_list_navigation' => __('Yatchs list navigation', 'yam'),
		'filter_items_list'     => __('Filter yatchs list', 'yam'),
	);
	$args = array(
		'label'               => __('Yatch', 'yam'),
		'description'         => __('Yatchs', 'yam'),
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type('yatchs', $args);
}
add_action('init', 'yatch_custom_post_type', 0);


// Register Custom Post Type
function contract_post_type()
{

	$labels = array(
		'name'                  => _x('Contracts', 'Post Type General Name', 'yam'),
		'singular_name'         => _x('Contract', 'Post Type Singular Name', 'yam'),
		'menu_name'             => __('Contracts', 'yam'),
		'name_admin_bar'        => __('Contract', 'yam'),
	);
	$args = array(
		'label'               => __('Contract', 'yam'),
		'description'         => __('Contracts', 'yam'),
		'labels'              => $labels,
		'supports'            => array('title', 'editor'),
		'taxonomies'          => array('contract_tag'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-text-page',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type('contract', $args);
}
add_action('init', 'contract_post_type', 0);

// Register Custom Taxonomy
function contract_tag_taxonomy()
{

	$labels = array(
		'name'                       => _x('Tags', 'Taxonomy General Name', 'yam'),
		'singular_name'              => _x('Tag', 'Taxonomy Singular Name', 'yam'),
		'menu_name'                  => __('Tags', 'yam'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
	);
	register_taxonomy('contract_tag', array('contract'), $args);
}
add_action('init', 'contract_tag_taxonomy', 0);

// Register Custom Post Type
function owner_post_type()
{

	$labels = array(
		'name'                  => _x('Owners', 'Post Type General Name', 'yam'),
		'singular_name'         => _x('Owner', 'Post Type Singular Name', 'yam'),
		'menu_name'             => __('Owners', 'yam'),
		'name_admin_bar'        => __('Owner', 'yam'),
	);
	$args = array(
		'label'               => __('Owner', 'yam'),
		'description'         => __('Owners', 'yam'),
		'labels'              => $labels,
		'supports'            => array('title', 'editor'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-businessman',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type('owner', $args);
}
add_action('init', 'owner_post_type', 0);

// Register Custom Post Type
function broker_post_type()
{

	$labels = array(
		'name'                  => _x('Brokers', 'Post Type General Name', 'yam'),
		'singular_name'         => _x('Broker', 'Post Type Singular Name', 'yam'),
		'menu_name'             => __('Brokers', 'yam'),
		'name_admin_bar'        => __('Broker', 'yam'),
	);
	$args = array(
		'label'               => __('Broker', 'yam'),
		'description'         => __('Brokers', 'yam'),
		'labels'              => $labels,
		'supports'            => array('title'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-portfolio',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type('broker', $args);
}
add_action('init', 'broker_post_type', 0);
