<?php

class Metaboxes_Broker
{
    public function __construct()
    {
        add_action('cmb2_admin_init', [$this, 'additional_data']);
    }

    public function additional_data()
    {
        $cmb = new_cmb2_box([
            'id'           => 'broker_additional_data',
            'title'        => esc_html__('Additional', 'yam'),
            'object_types' => ['broker'],
        ]);

        $cmb->add_field([
            'name'    => 'Agent Name',
            'desc'    => 'Enter the agent name of the broker',
            'id'      => '_agent_name',
            'type'    => 'text',
        ]);
    }
}

new Metaboxes_Broker();
