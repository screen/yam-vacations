<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */
load_theme_textdomain('yam', get_template_directory() . '/languages');
add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio'));
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('menus');
add_theme_support('customize-selective-refresh-widgets');
add_theme_support(
    'custom-background',
    array(
        'default-image' => '',
        'default-color' => 'ffffff',
        'wp-head-callback' => '_custom_background_cb',
        'admin-head-callback' => '',
        'admin-preview-callback' => ''
    )
);
add_theme_support('custom-logo', array(
    'height'      => 250,
    'width'       => 250,
    'flex-width'  => true,
    'flex-height' => true,
));
add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
));

/* ADD SHORTCODE SUPPORT TO TEXT WIDGETS */
add_filter('widget_text', 'do_shortcode');

/* --------------------------------------------------------------
    SECURITY ISSUES
-------------------------------------------------------------- */
/* REMOVE WORDPRESS VERSION */
function yam_remove_version()
{
    return '';
}
add_filter('the_generator', 'yam_remove_version');

/* CHANGE WORDPRESS ERROR ON LOGIN */
function yam_wordpress_errors()
{
    return __('Valores Incorrectos, intente de nuevo', 'yam');
}
add_filter('login_errors', 'yam_wordpress_errors');

/* DISABLE WORDPRESS RSS FEEDS */
function yam_disable_feed()
{
    wp_die(__('No hay RSS Feeds disponibles', 'yam'));
}

add_action('do_feed', 'yam_disable_feed', 1);
add_action('do_feed_rdf', 'yam_disable_feed', 1);
add_action('do_feed_rss', 'yam_disable_feed', 1);
add_action('do_feed_rss2', 'yam_disable_feed', 1);
add_action('do_feed_atom', 'yam_disable_feed', 1);

/* --------------------------------------------------------------
    IMAGES RESPONSIVE ON ATTACHMENT IMAGES
-------------------------------------------------------------- */
function image_tag_class($class)
{
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class');

/* --------------------------------------------------------------
    ADD NAV ITEM TO MENU AND LINKS
-------------------------------------------------------------- */
function special_nav_class($classes, $item)
{
    $classes[] = 'nav-item';
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function add_menuclass($ulclass)
{
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu', 'add_menuclass');

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_admin_styles()
{
    $version_remove = NULL;
    wp_register_style('wp-admin-style', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-admin-style');
}
add_action('login_head', 'custom_admin_styles');
add_action('admin_init', 'custom_admin_styles');

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGO
-------------------------------------------------------------- */

function yam_custom_logo()
{
    ob_start();
?>
    <style type="text/css">
        #wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before {
            background-image: url(<?php echo get_template_directory_uri('/');
                                    ?>/images/apple-touch-icon.png) !important;
            background-size: cover;
            background-position: 0 0;
            color: rgba(0, 0, 0, 0);
        }

        #wpadminbar #wp-admin-bar-wp-logo.hover>.ab-item .ab-icon {
            background-position: 0 0;
        }
    </style>
<?php
    $content = ob_get_clean();
    echo $content;
}

add_action('wp_before_admin_bar_render', 'yam_custom_logo');


/* --------------------------------------------------------------
    CUSTOM ADMIN FOOTER
-------------------------------------------------------------- */
function dashboard_footer()
{
    echo '<span id="footer-thankyou">';
    _e('Gracias por crear con ', 'yam');
    echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
    _e('Tema desarrollado por ', 'yam');
    echo '<a href="http://robertochoaweb.com/?utm_source=footer_admin&utm_medium=link&utm_content=yam" target="_blank">Robert Ochoa</a></span>';
}
add_filter('admin_footer_text', 'dashboard_footer');


/* --------------------------------------------------------------
    SOCIAL NETWORK SHORTCODE
-------------------------------------------------------------- */

add_shortcode('yam_social_networks', 'yam_social_networks_callback');
function yam_social_networks_callback()
{
    ob_start();
?>
    <?php $social_settings = get_option('yam_social_settings'); ?>
    <div class="social-widget-container">
        <?php if ($social_settings['instagram'] != '') { ?>
            <a href="<?php echo $social_settings['instagram']; ?>" title="<?php _e('Visita nuestro perfil en Instagram', 'yam'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
        <?php } ?>
        <?php if ($social_settings['facebook'] != '') { ?>
            <a href="<?php echo $social_settings['facebook']; ?>" title="<?php _e('Visita nuestro perfil en Facebook', 'yam'); ?>" target="_blank"><i class="fa fa-facebook-official"></i></a>
        <?php } ?>
        <?php if ($social_settings['twitter'] != '') { ?>
            <a href="<?php echo $social_settings['twitter']; ?>" title="<?php _e('Visita nuestro perfil en Twitter', 'yam'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        <?php } ?>
        <?php if ($social_settings['linkedin'] != '') { ?>
            <a href="<?php echo $social_settings['linkedin']; ?>" title="<?php _e('Visita nuestro perfil en LinkedIn', 'yam'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
        <?php } ?>
        <?php if ($social_settings['youtube'] != '') { ?>
            <a href="<?php echo $social_settings['youtube']; ?>" title="<?php _e('Visita nuestro perfil en Instagram', 'yam'); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
        <?php } ?>
    </div>
    <?php
    $content = ob_get_clean();
    return $content;
}

/* --------------------------------------------------------------
    CONTACT DATA WIDGET
-------------------------------------------------------------- */
class yam_contact_widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'yam_contact_widget', // Base ID
            esc_html__('YamVacations -> Contact Element', 'yam'), // Name
            array('description' => esc_html__('Widget que permite agregar elementos de contacto en el Pie de Página', 'yam'),) // Args
        );
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
    ?>

        <div class="contact-widget-element-container">
            <div class="contact-widget-element-icon">
                <?php if ($instance['element_type'] == 'dir') {
                    $class = 'address-icon';
                } ?>
                <?php if ($instance['element_type'] == 'phone') {
                    $class = 'phone-icon';
                } ?>
                <?php if ($instance['element_type'] == 'email') {
                    $class = 'email-icon';
                } ?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $class; ?>.png" alt="<?php echo $class; ?>" class="img-fluid" />
            </div>
            <div class="contact-widget-element-text">
                <?php echo apply_filters('the_content', $instance['element_text']); ?>
            </div>
        </div>
    <?php
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        $element_type = !empty($instance['element_type']) ? $instance['element_type'] : '';
        $element_text = !empty($instance['element_text']) ? $instance['element_text'] : '';
    ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('element_type')); ?>"><?php esc_attr_e('Tipo de elemento:', 'yam'); ?></label>
            <select class='widefat' id="<?php echo esc_attr($this->get_field_id('element_type')); ?>" name="<?php echo esc_attr($this->get_field_name('element_type')); ?>">
                <option value="dir" <?php selected($element_type, 'dir'); ?>><?php _e('Dirección', 'yam'); ?></option>
                <option value="phone" <?php selected($element_type, 'phone'); ?>><?php _e('Teléfonos', 'yam'); ?></option>
                <option value="email" <?php selected($element_type, 'email'); ?>><?php _e('Correo Electrónico', 'yam'); ?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('element_text')); ?>"><?php esc_attr_e('Texto del Elemento:', 'yam'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('element_text')); ?>" name="<?php echo esc_attr($this->get_field_name('element_text')); ?>" cols="30" rows="10"><?php echo $element_text; ?></textarea>
        </p>
    <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['element_type'] = (!empty($new_instance['element_type'])) ? sanitize_text_field($new_instance['element_type']) : '';
        $instance['element_text'] = (!empty($new_instance['element_text'])) ? $new_instance['element_text'] : '';

        return $instance;
    }
}

/* --------------------------------------------------------------
    CONTACT FORM SHORTCODE
-------------------------------------------------------------- */

add_shortcode('contact-form-shortcode', 'yam_contact_form_callback');
function yam_contact_form_callback()
{
    ob_start();
    ?>
    <?php get_template_part('templates/template-contact-form'); ?>
<?php
    $content = ob_get_clean();
    return $content;
}
