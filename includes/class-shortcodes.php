<?php
class Shortcodes
{

    public function __construct()
    {
        add_action('init', [$this, 'add_shortcodes']);
    }

    public function add_shortcodes()
    {
        add_shortcode('download_brochure_section', ['Brochure_Shortcode', 'download_brochure_section']);
    }
}
