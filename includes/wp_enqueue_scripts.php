<?php
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

function yam_load_scripts()
{
    $version_remove = null;
    if (!is_admin()) {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap-bundle', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array('jquery'), '4.5.3', true);
            wp_enqueue_script('bootstrap-bundle');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

        /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- AOS ON LOCAL -*/
            //wp_register_script('aos-js', get_template_directory_uri() . '/js/aos.js', array('jquery'), '3.0.0', true);
            //wp_enqueue_script('aos-js');
        } else {

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js', array('jquery'), '4.5.3', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script('sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.4/jquery.sticky.min.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- AOS -*/
            //wp_register_script('aos-js', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', array('jquery'), '2.3.4', true);
            //wp_enqueue_script('aos-js');
        }

        /*- SWIPER JS -*/
        wp_register_script('swiper-js', 'https://unpkg.com/swiper/swiper-bundle.min.js', array('jquery'), '6.1.2', true);
        wp_enqueue_script('swiper-js');

        /*- GOOGLE RECAPTCHA -*/
        $google_settings = get_option('yam_google_settings');
        if (isset($google_settings['sitekey'])) {
            wp_register_script('recaptcha-js', 'https://www.google.com/recaptcha/api.js', array('jquery'), '3.0.0', true);
            wp_enqueue_script('recaptcha-js');
        }

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');

        /*- MAIN FUNCTIONS -*/
        wp_register_script('form-functions', get_template_directory_uri() . '/js/form-functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('form-functions');

        /*- MAIN FUNCTIONS -*/
        if (is_singular('mphb_room_type')) {
            /*- AIR-DATEPICKER JS -*/
            wp_register_script('moment-datepicker-js', 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js', array('jquery'), '2.2.3', true);
            wp_enqueue_script('moment-datepicker-js');
            wp_register_script('air-datepicker-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.21.1/jquery.daterangepicker.min.js', array('jquery'), '2.2.3', true);
            wp_enqueue_script('air-datepicker-js');
            
            /* LOCATION FUNCTIONS */
            wp_register_script('location-functions', get_template_directory_uri() . '/js/location-functions.js', array('jquery'), $version_remove, true);
            wp_enqueue_script('location-functions');
        }

        /* LOCALIZE MAIN SHORTCODE SCRIPT */
        wp_localize_script('main-functions', 'custom_admin_url', array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'thanks_url' => home_url('/thanks-reservation'),
            'error_name' => __('Error: Name must not be empty', 'yam'),
            'invalid_name' => __('Error: You must enter a valid name', 'yam'),
            'error_email' => __('Error: Email address is empty', 'yam'),
            'invalid_email' => __('Error: Email address is invalid', 'yam'),
            'error_phone' => __('Error: Phone must not be empty', 'yam'),
            'invalid_phone' => __('Error: Phone is invalid', 'yam'),
            'success_form' => __('Thanks for your message! Soon you will be contacted.', 'yam'),
            'error_form' => __('Error: Please try again later.', 'yam')
        ));

        /*- WOOCOMMERCE OVERRIDES -*/
        if (class_exists('WooCommerce')) {
            wp_register_script('main-woocommerce-functions', get_template_directory_uri() . '/js/yam-woocommerce.js', array('jquery'), $version_remove, true);
            wp_enqueue_script('main-woocommerce-functions');
        }

        if (is_single('post') && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        } else {
            wp_deregister_script('comment-reply');
        }
    }
}

add_action('wp_enqueue_scripts', 'yam_load_scripts', 1);
