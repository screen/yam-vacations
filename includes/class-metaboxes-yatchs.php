<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Yam_Yatchs_Metaboxes')) :
    class Yam_Yatchs_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'yam_yatchs_custom_metabox'));
        }

        public function yam_yatchs_custom_metabox()
        {
            /* 1.- HOME: HERO SECTION */
            $cmb_yatchs_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'yatchs_metabox',
                'title'         => esc_html__('Yatchs Info', 'yam'),
                'object_types'  => array('yatchs'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_yatchs_metabox->add_field(array(
                'id'        => parent::PREFIX . 'yatchs_guests',
                'name'      => esc_html__('Number of Guests', 'yam'),
                'desc'      => esc_html__('Add the available number of guests for this yatch', 'yam'),
                'type'      => 'text'
            ));

            $cmb_yatchs_metabox->add_field(array(
                'id'            => parent::PREFIX . 'yatchs_gallery',
                'name'          => esc_html__('Gallery Images', 'yam'),
                'desc'          => esc_html__('Upload images for this gallery', 'yam'),
                'type'          => 'file_list',
                'preview_size'  => array(100, 100),
                'query_args'    => array('type' => 'image'),
                'text'          => array(
                    'add_upload_files_text' => 'Upload',
                    'remove_image_text'     => 'Remove',
                    'file_text'             => 'Image',
                    'file_download_text'    => 'Download',
                    'remove_text'           => 'Remove'
                )
            ));
        }
    }
endif;

new Yam_Yatchs_Metaboxes;
