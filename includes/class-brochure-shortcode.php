<?php

class Brochure_Shortcode
{
    /**
     * Return the brochure section
     *
     * @return string
     */
    public static function download_brochure_section()
    {
        $posts = get_posts([
            'post_type' => 'mphb_room_type',
            'posts_per_page' => -1,
        ]);

        ob_start();
        get_template_part('templates/brochure/shortcode-section', null, compact('posts'));
        $template  = ob_get_clean();

        return $template;
    }
}
