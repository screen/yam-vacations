<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Yam_Locations_Metaboxes')) :
    class Yam_Locations_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'yam_locations_custom_metabox'));
        }

        public function yam_locations_custom_metabox()
        {

            /* 0.- LOCATION - EXCLUSIVE */
            $cmb_location_exclusive_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'location_exclusive_metabox',
                'title'         => esc_html__('Highlight Location', 'yam'),
                'object_types'  => array('locations', 'mphb_room_type'),
                'context'       => 'side',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_location_exclusive_metabox->add_field(array(
                'id'            => parent::PREFIX . 'location_highlight',
                'name'          => esc_html__('Highlight Location?', 'yam'),
                'desc'          => esc_html__('Check this if you want to feature this location', 'yam'),
                'type'          => 'checkbox'
            ));

            /* 1.- LOCATION - MAIN INFO */
            $cmb_location_main_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'location_main_metabox',
                'title'         => esc_html__('Main Info', 'yam'),
                'object_types'  => array('locations', 'mphb_room_type'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_video',
                'name'      => esc_html__('Location Video', 'yam'),
                'desc'      => esc_html__('Add link to video for this location', 'yam'),
                'type'      => 'text_url'
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_price',
                'name'      => esc_html__('Location Price', 'yam'),
                'desc'      => esc_html__('Add the price for this location', 'yam'),
                'type'      => 'text_money'
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_map',
                'name'      => esc_html__('Location Embed Map', 'yam'),
                'desc'      => esc_html__('Add the Embed Map for this location', 'yam'),
                'type'      => 'textarea_code'
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_guests',
                'name'      => esc_html__('Guests', 'yam'),
                'desc'      => esc_html__('Add the available number of guests for this location', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '1',
                )
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_bedrooms',
                'name'      => esc_html__('Bedrooms', 'yam'),
                'desc'      => esc_html__('Add the available number of bedrooms for this location', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '1',
                )
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_bathrooms',
                'name'      => esc_html__('Bathrooms', 'yam'),
                'desc'      => esc_html__('Add the available number of bathrooms for this location', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '1',
                )
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_half_bathrooms',
                'name'      => esc_html__('Half-Bathrooms', 'yam'),
                'desc'      => esc_html__('Add the available number of half-bathrooms for this location', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '0',
                )
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_minimum_stay',
                'name'      => esc_html__('Minimumn Stay', 'yam'),
                'desc'      => esc_html__('Add the minimumn Stay required for this location', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '1',
                )
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_maximum_stay',
                'name'      => esc_html__('Maximum Stay', 'yam'),
                'desc'      => esc_html__('Add the maximum Stay required for this location', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '1',
                )
            ));

            $cmb_location_main_metabox->add_field(array(
                'id'        => parent::PREFIX . 'location_special',
                'name'      => esc_html__('Special Feature', 'yam'),
                'desc'      => esc_html__('Add a special feature for this location', 'yam'),
                'type'      => 'text'
            ));

            /* 1.- LOCATION - EXCLUSIVE */
            $cmb_location_bedtypes_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'location_bedtypes_metabox',
                'title'         => esc_html__('Bed Types', 'yam'),
                'object_types'  => array('locations', 'mphb_room_type'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $group_field_id = $cmb_location_bedtypes_metabox->add_field(array(
                'id'            => parent::PREFIX . 'location_bedtype_group',
                'name'          => esc_html__('Bed Types Group', 'yam'),
                'description'   => __('Servicios dentro del Item', 'yam'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Bedtype {#}', 'yam'),
                    'add_button'        => __('Add Other Bedtype', 'yam'),
                    'remove_button'     => __('Remove Bedtype', 'yam'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this Bedtype?', 'yam')
                )
            ));
            
            $cmb_location_bedtypes_metabox->add_group_field($group_field_id, array(
                'id'        => 'type',
                'name'      => esc_html__('Type', 'yam'),
                'desc'      => esc_html__('Select a type of bed for this location', 'yam'),
                'type'      => 'select',
                'default'   => 'single',
                'options'   => array(
                    'single'    => __('Single bed', 'yam'),
                    'double'    => __('Double bed', 'yam'),
                    'queen'    => __('Queen bed', 'yam'),
                    'king'    => __('King bed', 'yam')
                )
            ));
            
            $cmb_location_bedtypes_metabox->add_group_field($group_field_id, array(
                'id'        => 'quantity',
                'name'      => esc_html__('Quantity of beds', 'yam'),
                'desc'      => esc_html__('Enter a quantity of beds of this type', 'yam'),
                'type'      => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min'  => '0',
                )
            ));
            

            /* 2.- LOCATION - GALLERY IMAGES */
            $cmb_location_gallery_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'location_gallery_metabox',
                'title'         => esc_html__('Gallery Images', 'yam'),
                'object_types'  => array('locations'),
                'context'       => 'side',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_location_gallery_metabox->add_field(array(
                'id'            => parent::PREFIX . 'location_gallery',
                'name'         => esc_html__('Select gallery images', 'yam'),
                'type'          => 'file_list',
                'preview_size'  => array(50, 50),
                'query_args'    => array('type' => 'image'),
                'text'          => array(
                    'add_upload_files_text' => 'Upload',
                    'remove_image_text'     => 'Remove',
                    'file_text'             => 'Image',
                    'file_download_text'    => 'Download',
                    'remove_text'           => 'Remove'
                )
            ));

            /* 3.- LOCATION - POLICIES INFO */
            $cmb_location_policies_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'location_policies_metabox',
                'title'         => esc_html__('Policies', 'yam'),
                'object_types'  => array('locations', 'mphb_room_type'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $group_field_id = $cmb_location_policies_metabox->add_field(array(
                'id'            => parent::PREFIX . 'location_policies_group',
                'name'          => esc_html__('Policies Group', 'yam'),
                'description'   => __('Policies Items inside Locations', 'yam'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Policy {#}', 'yam'),
                    'add_button'        => __('Add other Policy', 'yam'),
                    'remove_button'     => __('Remove Policy', 'yam'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this policy?', 'yam')
                )
            ));

            $cmb_location_policies_metabox->add_group_field($group_field_id, array(
                'id'        => 'icon',
                'name'      => esc_html__('Policy Icon Type', 'yam'),
                'desc'      => esc_html__('Select icon type for this policy', 'yam'),
                'type'      => 'select',
                'default'   => 'check',
                'options'   => array(
                    'check' => 'Checkmark Icon',
                    'cancel' => 'Cancel Icon'
                )
            ));

            $cmb_location_policies_metabox->add_group_field($group_field_id, array(
                'id'        => 'title',
                'name'      => esc_html__('Policy Text', 'yam'),
                'desc'      => esc_html__('Insert Policy Text', 'yam'),
                'type'      => 'text'
            ));

            /* 4.- LOCATION - ADDITIONAL RULES */
            $cmb_location_rules_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'location_rules_metabox',
                'title'         => esc_html__('Additional Rules', 'yam'),
                'object_types'  => array('locations', 'mphb_room_type'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_location_rules_metabox->add_field(array(
                'id'        => parent::PREFIX . 'additional_rules',
                'name'      => esc_html__('Additional Rules', 'yam'),
                'desc'      => esc_html__('Add some additional rules if needed', 'yam'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));
        }
    }
endif;

new Yam_Locations_Metaboxes;