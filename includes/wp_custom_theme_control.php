<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action('customize_register', 'yam_customize_register');

function yam_customize_register($wp_customize)
{
    $wp_customize->add_setting('white_logo', array(
        'transport' => 'refresh',
        'height' => 70,
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'white_logo_image',
            array(
                'label'      => __('Upload a white logo version', 'yam'),
                'description' => __('This logo will be posted on home only.', 'yam'),
                'section'    => 'title_tagline',
                'settings'   => 'white_logo'
            )
        )
    );

    /* HEADER */
    $wp_customize->add_section('yam_header_settings', array(
        'title'    => __('Header', 'yam'),
        'description' => __('Header elements options', 'yam'),
        'priority' => 30
    ));

    $wp_customize->add_setting('yam_header_settings[header_left]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('header_left', array(
        'type' => 'text',
        'label'    => __('Top Header Text [Left]', 'yam'),
        'description' => __('Add the descriptive text for the header left section', 'yam'),
        'section'  => 'yam_header_settings',
        'settings' => 'yam_header_settings[header_left]'
    ));

    $wp_customize->add_setting('yam_header_settings[header_phone_link]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('header_phone_link', array(
        'type' => 'text',
        'label'    => __('Top Header Phone [Link]', 'yam'),
        'description' => __('Add the descriptive link for the header phone section', 'yam'),
        'section'  => 'yam_header_settings',
        'settings' => 'yam_header_settings[header_phone_link]'
    ));

    $wp_customize->add_setting('yam_header_settings[header_phone_text]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('header_phone_text', array(
        'type' => 'text',
        'label'    => __('Top Header Phone [Text]', 'yam'),
        'description' => __('Add the descriptive text for the header phone section', 'yam'),
        'section'  => 'yam_header_settings',
        'settings' => 'yam_header_settings[header_phone_text]'
    ));

    $wp_customize->add_setting('yam_header_settings[header_right]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('header_right', array(
        'type' => 'text',
        'label'    => __('Top Header Text [Right]', 'yam'),
        'description' => __('Add the descriptive text for the header right section', 'yam'),
        'section'  => 'yam_header_settings',
        'settings' => 'yam_header_settings[header_right]'
    ));

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('yam_social_settings', array(
        'title'    => __('Redes Sociales', 'yam'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'yam'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('yam_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'yam_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('facebook', array(
        'type' => 'url',
        'section' => 'yam_social_settings',
        'settings' => 'yam_social_settings[facebook]',
        'label' => __('Facebook', 'yam'),
    ));

    $wp_customize->add_setting('yam_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'yam_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('twitter', array(
        'type' => 'url',
        'section' => 'yam_social_settings',
        'settings' => 'yam_social_settings[twitter]',
        'label' => __('Twitter', 'yam'),
    ));

    $wp_customize->add_setting('yam_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'yam_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('instagram', array(
        'type' => 'url',
        'section' => 'yam_social_settings',
        'settings' => 'yam_social_settings[instagram]',
        'label' => __('Instagram', 'yam'),
    ));

    $wp_customize->add_setting('yam_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'yam_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('linkedin', array(
        'type' => 'url',
        'section' => 'yam_social_settings',
        'settings' => 'yam_social_settings[linkedin]',
        'label' => __('LinkedIn', 'yam'),
    ));

    $wp_customize->add_setting('yam_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'yam_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('youtube', array(
        'type' => 'url',
        'section' => 'yam_social_settings',
        'settings' => 'yam_social_settings[youtube]',
        'label' => __('YouTube', 'yam'),
    ));

    /* COOKIES SETTINGS */
    $wp_customize->add_section('yam_cookie_settings', array(
        'title'    => __('Cookies', 'yam'),
        'description' => __('Opciones de Cookies', 'yam'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('yam_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'yam'),
        'description' => __('Texto del Cookie consent.'),
        'section'  => 'yam_cookie_settings',
        'settings' => 'yam_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('yam_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'yam_cookie_settings',
        'settings' => 'yam_cookie_settings[cookie_link]',
        'label' => __('Link de Cookies', 'yam'),
    ));

    /* SENDINBLUE SETTINGS */
    $wp_customize->add_section('yam_sendinblue_settings', array(
        'title'    => __('Sendinblue', 'yam'),
        'description' => __('Opciones para Sendinblue', 'yam'),
        'priority' => 180
    ));

    $wp_customize->add_setting('yam_sendinblue_settings[apikey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('apikey', array(
        'type' => 'text',
        'label'    => __('APIKey', 'yam'),
        'description' => __('Agregar APIkey de Sendinblue', 'yam'),
        'section'  => 'yam_sendinblue_settings',
        'settings' => 'yam_sendinblue_settings[apikey]'
    ));

    $wp_customize->add_setting('yam_sendinblue_settings[footer_list_id]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('footer_list_id', array(
        'type'     => 'number',
        'section' => 'yam_sendinblue_settings',
        'settings' => 'yam_sendinblue_settings[footer_list_id]',
        'label' => __('List ID for Footer Contact', 'yam')
    ));

    $wp_customize->add_setting('yam_sendinblue_settings[concierges_list_id]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('concierges_list_id', array(
        'type'     => 'number',
        'section' => 'yam_sendinblue_settings',
        'settings' => 'yam_sendinblue_settings[concierges_list_id]',
        'label' => __('List ID for Concierges', 'yam')
    ));

    $wp_customize->add_setting('yam_sendinblue_settings[reservation_list_id]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('reservation_list_id', array(
        'type'     => 'number',
        'section' => 'yam_sendinblue_settings',
        'settings' => 'yam_sendinblue_settings[reservation_list_id]',
        'label' => __('List ID for Reservation', 'yam')
    ));

    /* GOOGLE RECAPTCHA */
    $wp_customize->add_section('yam_google_settings', array(
        'title'    => __('Google', 'yam'),
        'description' => __('Google Recaptcha Data', 'yam'),
        'priority' => 180,
    ));

    $wp_customize->add_setting('yam_google_settings[sitekey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('sitekey', array(
        'type' => 'text',
        'label'    => __('SiteKey', 'yam'),
        'description' => __('Enter Recaptcha API SiteKey.'),
        'section'  => 'yam_google_settings',
        'settings' => 'yam_google_settings[sitekey]'
    ));

    $wp_customize->add_setting('yam_google_settings[secretkey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('secretkey', array(
        'type' => 'text',
        'label'    => __('SecretKey', 'yam'),
        'description' => __('Enter Recaptcha API SecretKey.'),
        'section'  => 'yam_google_settings',
        'settings' => 'yam_google_settings[secretkey]'
    ));
}

function yam_sanitize_url($url)
{
    return esc_url_raw($url);
}

/* --------------------------------------------------------------
CUSTOM CONTROL PANEL
-------------------------------------------------------------- */
/*
function register_yam_settings() {
    register_setting( 'yam-settings-group', 'monday_start' );
    register_setting( 'yam-settings-group', 'monday_end' );
    register_setting( 'yam-settings-group', 'monday_all' );
}

add_action('admin_menu', 'yam_custom_panel_control');

function yam_custom_panel_control() {
    add_menu_page(
        __( 'Panel de Control', 'yam' ),
        __( 'Panel de Control','yam' ),
        'manage_options',
        'yam-control-panel',
        'yam_control_panel_callback',
        'dashicons-admin-generic',
        120
    );
    add_action( 'admin_init', 'register_yam_settings' );
}

function yam_control_panel_callback() {
    ob_start();
?>
<div class="yam-admin-header-container">
    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="yam" />
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
</div>
<form method="post" action="options.php" class="yam-admin-content-container">
    <?php settings_fields( 'yam-settings-group' ); ?>
    <?php do_settings_sections( 'yam-settings-group' ); ?>
    <div class="yam-admin-content-item">
        <table class="form-table">
            <tr valign="center">
                <th scope="row"><?php _e('Monday', 'yam'); ?></th>
                <td>
                    <label for="monday_start">Starting Hour: <input type="time" name="monday_start" value="<?php echo esc_attr( get_option('monday_start') ); ?>"></label>
                    <label for="monday_end">Ending Hour: <input type="time" name="monday_end" value="<?php echo esc_attr( get_option('monday_end') ); ?>"></label>
                    <label for="monday_all">All Day: <input type="checkbox" name="monday_all" value="1" <?php checked( get_option('monday_all'), 1 ); ?>></label>
                </td>
            </tr>
        </table>
    </div>
    <div class="yam-admin-content-submit">
        <?php submit_button(); ?>
    </div>
</form>
<?php
    $content = ob_get_clean();
    echo $content;
}
*/
