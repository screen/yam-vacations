<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Property: {property_name}</title>
    <style type="text/css" media="all">
        html {
            margin: 0;
        }

        body {
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            margin: 4mm 5mm 3mm 5mm;
        }
    </style>
</head>

<body>
    <h1 style="font-weight: 600;">{property_name}</h1>
    <br />
    <img src="{property_thumbnail}" width="600" height="auto" />
    <br />
    <h2 style="font-weight: 600;">Property Overview</h2>
    <p><b>Location:</b> {property_location}</p>
    <p><b>Category:</b> {property_category}</p>
    <p><b>Guests:</b> {property_guests}</p>
    <p><b>Bedrooms:</b> {property_bedrooms}</p>
    <p><b>Bathrooms:</b> {property_bathrooms}</p>
    <p><b>Half-Bathrooms:</b> {property_half_bathrooms}</p>
    <p><b>Minimum Stay:</b> {property_minimum} - {property_maximum}</p>
    <p><b>Special:</b> {property_special}</p>
    <br />
    {property_content}
    <br />
    <h2 style="font-weight: 600;">Property Amenities</h2>
    {property_amenities}
    <br />
    <h2 style="font-weight: 600;">Property Policies</h2>
    {property_policies}
    <br />
    <h3 style="font-weight: 600;">Additional Rules</h3>
    {property_additional_rules}
    <br />
    <h2 style="font-weight: 600;">Near By</h2>
    {property_nearby}
    <br />
</body>

</html>