<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Yam_Home_Metaboxes')) :
    class Yam_Home_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'yam_home_custom_metabox'));
        }

        public function yam_home_custom_metabox()
        {
            /* 1.- HOME: HERO SECTION */
            $cmb_home_hero = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_hero_metabox',
                'title'         => esc_html__('Home: Main Slider Hero', 'yam'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-home.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_hero->add_field(array(
                'id'            => parent::PREFIX . 'slider_images',
                'name'          => esc_html__('Images from Slider', 'yam'),
                'desc'          => esc_html__('Upload images for Hero Slider in Home', 'yam'),
                'type'          => 'file_list',
                'preview_size'  => array(100, 100),
                'query_args'    => array('type' => 'image'),
                'text'          => array(
                    'add_upload_files_text' => 'Upload',
                    'remove_image_text'     => 'Remove',
                    'file_text'             => 'Image',
                    'file_download_text'    => 'Download',
                    'remove_text'           => 'Remove'
                )
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'slider_title',
                'name'      => esc_html__('Slider Title', 'yam'),
                'desc'      => esc_html__('Add a descriptive title for this slider', 'yam'),
                'type'      => 'text'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'slider_desc',
                'name'      => esc_html__('Slider Description', 'yam'),
                'desc'      => esc_html__('Add a description below title for this slider', 'yam'),
                'type'      => 'textarea'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'slider_btn_text',
                'name'      => esc_html__('Slider Button Text', 'yam'),
                'desc'      => esc_html__('Add a descriptive text for this button', 'yam'),
                'type'      => 'text'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'slider_btn_link',
                'name'      => esc_html__('Slider Button Link', 'yam'),
                'desc'      => esc_html__('Add a descriptive link url for this button', 'yam'),
                'type'      => 'text_url'
            ));

            $cmb_home_hero->add_field(array(
                'id'            => parent::PREFIX . 'slider_logos',
                'name'          => esc_html__('Logos from Slider', 'yam'),
                'desc'          => esc_html__('Upload logo for Hero Slider in Home', 'yam'),
                'type'          => 'file_list',
                'preview_size'  => array(100, 100),
                'query_args'    => array('type' => 'image'),
                'text'          => array(
                    'add_upload_files_text' => 'Upload',
                    'remove_image_text'     => 'Remove',
                    'file_text'             => 'Image',
                    'file_download_text'    => 'Download',
                    'remove_text'           => 'Remove'
                )
            ));

            /* 2.- HOME: VIDEO SECTION */
            $cmb_home_video = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_video_metabox',
                'title'         => esc_html__('Home: Video Hero', 'yam'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-home.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_video->add_field(array(
                'id'        => parent::PREFIX . 'home_video',
                'name'      => esc_html__('Background Video for Hero', 'yam'),
                'desc'      => esc_html__('Upload a Video for this section', 'yam'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload video', 'yam'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                        'video/webp',
                        'video/ogv'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            /* 3.- HOME: BENEFITS SECTION */
            $cmb_home_benefits = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_benfits_metabox',
                'title'         => esc_html__('Home: Benefits Section', 'yam'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-home.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_benefits->add_field(array(
                'id'        => parent::PREFIX . 'home_benefits_image',
                'name'      => esc_html__('Image for Hero', 'yam'),
                'desc'      => esc_html__('Upload an image for this section', 'yam'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload image', 'yam'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_benefits->add_field(array(
                'id'        => parent::PREFIX . 'home_benefits_content',
                'name'      => esc_html__('Section Content', 'yam'),
                'desc'      => esc_html__('Insert the description of this content', 'yam'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_benefits->add_field(array(
                'id'        => parent::PREFIX . 'benefits_btn_text',
                'name'      => esc_html__('Section Button Text', 'yam'),
                'desc'      => esc_html__('Add a descriptive text for this button', 'yam'),
                'type'      => 'text'
            ));

            $cmb_home_benefits->add_field(array(
                'id'        => parent::PREFIX . 'benefits_btn_link',
                'name'      => esc_html__('Section Button Link', 'yam'),
                'desc'      => esc_html__('Add a descriptive link url for this button', 'yam'),
                'type'      => 'text_url'
            ));

            /* 5.- HOME: ABOUT SECTION */
            $cmb_home_about = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_about_metabox',
                'title'         => esc_html__('Home: About Section', 'yam'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-home.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_about->add_field(array(
                'id'            => parent::PREFIX . 'home_about_slider_images',
                'name'          => esc_html__('Images from Slider', 'yam'),
                'desc'          => esc_html__('Upload images for About Slider in Home', 'yam'),
                'type'          => 'file_list',
                'preview_size'  => array(100, 100),
                'query_args'    => array('type' => 'image'),
                'text'          => array(
                    'add_upload_files_text' => 'Upload',
                    'remove_image_text'     => 'Remove',
                    'file_text'             => 'Image',
                    'file_download_text'    => 'Download',
                    'remove_text'           => 'Remove'
                )
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_content',
                'name'      => esc_html__('Section Content', 'yam'),
                'desc'      => esc_html__('Insert the description of this content', 'yam'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));
        }
    }
endif;

new Yam_Home_Metaboxes;
