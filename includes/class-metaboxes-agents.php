<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Yam_Agents_Metaboxes')) :
    class Yam_Agents_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'yam_agents_custom_metabox'));
        }

        public function yam_agents_custom_metabox()
        {
            $arr_rooms = array();
            $arr_type_room = new WP_Query(array('post_type' => 'mphb_room_type', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date'));
            if ($arr_type_room->have_posts()) :
                while ($arr_type_room->have_posts()) : $arr_type_room->the_post();
                    $arr_rooms[get_the_ID()] = get_the_title();
                endwhile;
            endif;
            wp_reset_query();



            /* 1.- HOME: HERO SECTION */
            $cmb_agents_metabox = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'agents_metabox',
                'title'         => esc_html__('Agents: Main Download Links', 'yam'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-agents.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $group_field_id = $cmb_agents_metabox->add_field(array(
                'id'            => parent::PREFIX . 'agents_links_group',
                'name'          => esc_html__('Links Group', 'yam'),
                'description'   => __('Download links inside this section', 'yam'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Link {#}', 'yam'),
                    'add_button'        => __('Add other link', 'yam'),
                    'remove_button'     => __('Remove Link', 'yam'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('¿Estas seguro de remover este Item?', 'yam')
                )
            ));

            $cmb_agents_metabox->add_group_field($group_field_id, array(
                'id'        => 'title',
                'name'      => esc_html__('Link Title', 'yam'),
                'desc'      => esc_html__('Insert a descriptive title for this link', 'yam'),
                'type'      => 'text'
            ));

            $cmb_agents_metabox->add_group_field($group_field_id, array(
                'id'        => 'link',
                'name'      => esc_html__('Link URL', 'yam'),
                'desc'      => esc_html__('Put a file for download or add link to this file', 'yam'),
                'type'      => 'file',

                'options' => array(
                    'url' => true
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload File or Insert Link', 'yam'),
                ),
            ));

            $cmb_agents_metabox->add_group_field($group_field_id, array(
                'id'        => 'activate',
                'name'      => esc_html__('Activate PDF by Room', 'yam'),
                'desc'      => esc_html__('Select this if this PDF will be automatically generated', 'yam'),
                'type'      => 'checkbox'
            ));

            $cmb_agents_metabox->add_group_field($group_field_id, array(
                'id'        => 'room_id',
                'name'      => esc_html__('Select Room', 'yam'),
                'desc'      => esc_html__('Select the room associated to this button link', 'yam'),
                'type'      => 'select',
                'options'   => $arr_rooms
            ));
        }
    }
endif;

new Yam_Agents_Metaboxes;
