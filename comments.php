<ul class="comment-list comments">
    <?php $args = array('style' => 'ul', 'short_ping' => true, 'callback' => 'better_comments'); ?>
    <?php wp_list_comments($args); ?>
    <?php paginate_comments_links(); ?>
</ul><!-- .comment-list -->
<div class="write-review-container">
    <button id="writeReview" class="btn btn-md btn-review"><?php _e('Write a Review', 'yam'); ?></button>
    <div class="review-form-container d-none">
        <?php comment_form(); ?>
    </div>
</div>