<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special p-0">
                <div class="row no-gutters align-items-center">
                    <?php if (is_active_sidebar('sidebar_footer')) : ?>
                        <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                            <ul id="sidebar-footer1" class="footer-sidebar">
                                <?php dynamic_sidebar('sidebar_footer'); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if (is_active_sidebar('sidebar_footer-2')) : ?>
                        <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                            <ul id="sidebar-footer2" class="footer-sidebar">
                                <?php dynamic_sidebar('sidebar_footer-2'); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if (is_active_sidebar('sidebar_footer-3')) : ?>
                        <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                            <ul id="contact" class="footer-sidebar">
                                <?php dynamic_sidebar('sidebar_footer-3'); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <div class="w-100"></div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="footer-copy-left col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright &copy; 2021 - YAM Vacations Rentals
                    </div>
                    <div class="footer-copy-right col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php printf(__('DEVELOPED BY <a href="%s">SMG</a> | <a href="%s">DIGITAL MARKETING AGENCY</a>', 'yam'), 'http://www.screenmediagroup.com', 'http://www.screenmediagroup.com'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<?php wp_footer() ?>

<?php get_template_part('templates/brochure/get-brochure-modal') ?>

</body>

</html>