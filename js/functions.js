var menuBtn = '';
var menuBtnCloser = '';
var menuMobile = '';
var menuHeader = '';

var footerSidebar1 = '';
var footerSidebar2 = '';
var footerSidebar3 = '';

var windowWidth = 0;


/* CUSTOM ON LOAD FUNCTIONS */
function yamCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    /* MENU MOBILE BEHAVIOR */
    menuBtn = document.getElementById('menuBtn');
    menuMobile = document.getElementById('menuCntr');
    menuHeader = document.getElementById('headerMenu');

    menuBtn.addEventListener('click', function() {
        this.classList.toggle('open');
        menuMobile.classList.toggle('show-menu');
        menuHeader.classList.toggle('showed-menu');
    });

    /* EQUALIZE FOOTER HEIGHTS */
    windowWidth = window.clientWidth;
    reportWindowSize();
    window.addEventListener('resize', reportWindowSize);


    /* SMALL MENU ON CONCIERGES */
    minimizeMenuConcierges();
    jQuery(window).resize(function() {
        minimizeMenuConcierges();
    });

    /* BUTTON PLAY ON FEATURED LOCATION */
    jQuery('.img-play').on('click', function(e) {
        e.preventDefault();
        var video_url = jQuery(this).data('video');
        jQuery('#embedModalVideo').attr('src', video_url);
        jQuery('#homeVideoModal').modal('toggle');
    });

    jQuery('#homeVideoModal').on('hidden.bs.modal', function(event) {
        jQuery('#embedModalVideo').attr('src', '');
    });

    /* GENERATE PDF FILE FOR AGENTS */
    jQuery('.generate-pdf').on('click', function(e) {
        e.preventDefault();
        jQuery.ajax({
            type: 'POST',
            url: custom_admin_url.ajax_url,
            data: {
                action: 'create_report_by_location',
                id: jQuery(this).data('roomid')
            },
            success: function(response) {
                var urlFile = decodeURIComponent(response);
                urlFile = urlFile.replace(/['"]+/g, '');
                var link = document.createElement('a');
                link.href = urlFile;
                link.download = 'property.pdf';
                document.body.appendChild(link);
                link.click();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
}

document.addEventListener("DOMContentLoaded", yamCustomLoad, false);

/* CHANGE FOOTER HEIGHTS */
function reportWindowSize() {
    footerSidebar1 = document.getElementById('sidebar-footer1');
    footerSidebar2 = document.getElementById('sidebar-footer2');
    footerSidebar3 = document.getElementById('contact');
    var footerHeight = footerSidebar1.clientHeight;
    windowWidth = window.clientWidth;
    if (windowWidth <= 767) {
        footerSidebar1.style.height = 'auto';
        footerSidebar2.style.height = 'auto';
        footerSidebar3.style.height = 'auto';
    } else {
        footerSidebar2.style.height = footerHeight + 'px';
        footerSidebar3.style.height = footerHeight + 'px';
    }
}

/* CHANGE MENU CONCIERGES */
function minimizeMenuConcierges() {
    var menuConcierges = parseInt(jQuery('.menu-concierges').width() - 150);
    var menuConciergesLi = 0;

    jQuery('#menu_ul li').each(function() {
        jQuery(this).removeClass('d-none');
    });

    jQuery('#menu_ul li').each(function() {
        menuConciergesLi = menuConciergesLi + jQuery(this).width();

        var lastId = jQuery(this).attr('id').slice(0, -4);
        if (parseInt(menuConciergesLi) > menuConcierges) {
            jQuery(this).addClass('d-none');
            jQuery('#' + lastId + 'Over').removeClass('d-none');
        } else {
            jQuery(this).removeClass('d-none');
            jQuery('#' + lastId + 'Over').addClass('d-none');
        }

    });
}

/* SWIPER HOME MAIN SLIDER */
var mainHomeSwiper = new Swiper('.home-swiper-container', {
    direction: 'horizontal',
    loop: true,
    slidesPerView: 1,
    spaceBetween: 10,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
});

/* SWIPER HOME ABOUT */
var aboutHomeSwiper = new Swiper('.about-swiper-container', {
    direction: 'horizontal',
    loop: true,
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
});