var isTaken = false;
var windowWidth = 0;
/* CUSTOM ON LOAD FUNCTIONS */
function yamLocationsCustomLoad() {
    "use strict";
    console.log('Functions Location Correctly Loaded');
    var menuGuest = document.getElementById('guestSelector');
    
    if (menuGuest) {
        menuGuest.addEventListener('click', function() {
            menuGuest.classList.toggle('guest-dropdown-open');
            var guestElement = document.getElementsByClassName('guest-selector-menu');
            guestElement[0].classList.toggle('d-none');
        });
    }

    var menuGuestMobile = document.getElementById('guestSelectorMobile');
    if (menuGuestMobile) {
        menuGuestMobile.addEventListener('click', function() {
            menuGuestMobile.classList.toggle('guest-dropdown-open');
            var guestElementMobile = document.getElementsByClassName('guest-selector-menu');
            guestElementMobile[1].classList.toggle('d-none');
        });
    }

    jQuery('#shareLink').on('click', function(e) {
        e.preventDefault();
        jQuery('#shareMenu').toggleClass('share-menu-hidden');
    });

    jQuery('#demo').dateRangePicker({
        autoClose: false,
        format: 'MM-DD-YYYY',
        separator: ' ',
        language: 'auto',
        startOfWeek: 'sunday', // or monday
        getValue: function() {
            return $(this).val();
        },
        setValue: function(s) {
            if (!$(this).attr('readonly') && !$(this).is(':disabled') && s != $(this).val()) {
                $(this).val(s);
            }
        },
        container: '.modal-date-container',
        showTopbar: false,
        startDate: moment().format('MM-DD-YYYY'),
        endDate: false,
        time: {
            enabled: false
        },
        minDays: 3,
        inline: true,
        alwaysOpen: true,
        lookBehind: false,
        batchMode: false,

    }).bind('datepicker-change', function(event, obj) {
        var dateStart = formatDate(obj.date1);
        var dateEnd = formatDate(obj.date2);

        jQuery('input[name=daysQty]').val((datediff(parseDate(dateStart), parseDate(dateEnd)) + 1));

        jQuery('#modalDateIn').html(dateStart);
        jQuery('#modalDateOut').html(dateEnd);



        jQuery.ajax({
            type: 'POST',
            url: custom_admin_url.ajax_url,
            data: {
                action: 'check_location_reservation',
                locationID: jQuery('input[name=propertyID]').val(),
                checkInDate: dateStart,
                checkOutDate: dateEnd,
                diffDays: jQuery('input[name=daysQty]').val()
            },
            beforeSend: function() {
                jQuery('.modal-response-container').removeClass('d-none');
                jQuery('.modal-response-container').html('<div class="lds-ripple"><div></div><div></div></div>');
            },
            success: function(response) {
                var respuesta = JSON.parse(response);
                isTaken = respuesta.taken;
                if (respuesta.taken == false) {
                    jQuery('.modal-response-container').html('<span class="false-taken">' + 'Dates are available' + '</span>');
                } else {
                    jQuery('.modal-response-container').html('<span class="true-taken">' + 'Dates are unvailable, please take another range' + '</span>');
                }
                console.log(respuesta);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    jQuery('.sticker-data').sticky({
        topSpacing: 40,
        bottomSpacing: footerSidebar1.clientHeight + 150
    });


    jQuery(window).resize(function() {
        windowWidth = jQuery(document).width();
        console.log();

        if (windowWidth > 767) {
            jQuery('.sticker-data').sticky({
                topSpacing: 40,
                bottomSpacing: footerSidebar1.clientHeight + 150
            });
        } else {
            jQuery('.sticker-data').unstick();
        }

        var menuProperties = (jQuery('.property-navigator').width() - 250);
        var menuPropertiesLi = 0;

        jQuery('#menu_ul_properties li').each(function() {
            jQuery(this).removeClass('d-none');
        });

        jQuery('#menu_ul_properties li').each(function() {
            menuPropertiesLi = menuPropertiesLi + jQuery(this).width();

            var lastId = jQuery(this).attr('id').slice(0, -4);
            if (menuPropertiesLi > menuProperties) {
                jQuery(this).addClass('d-none');
                jQuery('#' + lastId + 'over').removeClass('d-none');
            } else {
                jQuery(this).removeClass('d-none');
                jQuery('#' + lastId + 'over').addClass('d-none');
            }

        });
    });

    var menuProperties = (jQuery('.property-navigator').width() - 250);
    var menuPropertiesLi = 0;
    jQuery('#menu_ul_properties li').each(function() {
        menuPropertiesLi = menuPropertiesLi + jQuery(this).width();

        jQuery('#menu_ul_properties li').each(function() {
            jQuery(this).removeClass('d-none');
        });

        var lastId = jQuery(this).attr('id').slice(0, -4);
        if (menuPropertiesLi > menuProperties) {
            jQuery(this).addClass('d-none');
            jQuery('#' + lastId + 'over').removeClass('d-none');
        } else {
            jQuery(this).removeClass('d-none');
            jQuery('#' + lastId + 'over').addClass('d-none');
        }

    });



    jQuery('.plus').on('click', function() {
        var current = 0;
        var currentID = jQuery(this).prev().attr('id');
        console.log(currentID);
        current = parseInt(jQuery(this).prev('input').val());
        current = current + 1;
        if (current > 10) {
            current = 10;
        }
        jQuery(this).prev('input').val(current);
        calculateGuests();
        calculateGuestsMobile();
    });

    jQuery('.minus').on('click', function() {
        var current = 0;
        var currentID = jQuery(this).prev().attr('id');
        console.log(currentID);
        current = parseInt(jQuery(this).next('input').val());
        current = current - 1;
        if (current < 0) {
            current = 0;
        }
        jQuery(this).next('input').val(current);
        calculateGuests();
        calculateGuestsMobile();
    });




    jQuery('.btn-availability').on('click', function(e) {
        e.preventDefault();

        if (jQuery('#dateIn').val() == '') {
            jQuery('.date-guests-response').html('<span class="error-message">you must select your dates and guests</span>');
            return;
        }
        if (jQuery('#dateOut').val() == '') {
            jQuery('.date-guests-response').html('<span class="error-message">you must select your dates and guests</span>');
            return;
        }
        if ((jQuery('input[name=allGuests]').val() == '') || (jQuery('input[name=allGuests]').val() == 0)) {
            jQuery('.date-guests-response').html('<span class="error-message">You must select your dates and guests</span>');
            return;
        }

        calculatePricing();

    });



    jQuery('.btn-reservation').on('click', function(e) {
        e.preventDefault();

        calculatePricing();
        jQuery('.sticker-data-wrapper').addClass('d-none');
        jQuery('.confirm-reservation').removeClass('d-none');
    });

    jQuery('.btn-availability-mobile').on('click', function(e) {
        e.preventDefault();

        jQuery('#dateModal').modal('toggle');
    });


    jQuery('.btn-reserve-mobile').on('click', function(e) {
        e.preventDefault();
        calculatePricingMobile();
        jQuery('#mobileModal').modal('toggle');
    });

    jQuery('.btn-complete-form').on('click', function(e) {
        e.preventDefault();
        jQuery.ajax({
            type: 'POST',
            url: custom_admin_url.ajax_url,
            data: {
                action: 'make_complete_reservation',
                info: jQuery('#mainReservationForm').serialize()
            },
            success: function(response) {
                window.location.href = custom_admin_url.thanks_url;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    jQuery('.btn-save').on('click', function(e) {
        e.preventDefault();
        if (isTaken == false) {
            jQuery('#dateInLabel').html(jQuery('#modalDateIn').html());
            jQuery('#dateOutLabel').html(jQuery('#modalDateOut').html());
            jQuery('#dateIn').val(jQuery('#modalDateIn').html());
            jQuery('#dateOut').val(jQuery('#modalDateOut').html());

            jQuery('#dateInMobile').val(jQuery('#modalDateIn').html());
            jQuery('#dateOutMobile').val(jQuery('#modalDateOut').html());
            jQuery('#dateInLabelMobile').html(jQuery('#modalDateIn').html());
            jQuery('#dateOutLabelMobile').html(jQuery('#modalDateOut').html());
        }
        jQuery('.btn-availability-mobile').addClass('d-none');
        jQuery('.btn-reserve-mobile').removeClass('d-none');
        jQuery('#dateModal').modal('toggle');

        calculatePricing();
    });

    jQuery('#writeReview').on('click', function(e) {
        e.preventDefault();
        jQuery('.review-form-container').toggleClass('d-none');
    });
}

document.addEventListener("DOMContentLoaded", yamLocationsCustomLoad, false);


function calculateGuests() {
    var adults = parseInt(jQuery('input[name=guest-adults]').val());
    var youth = parseInt(jQuery('input[name=guest-youth]').val());
    var allGuests = adults + youth;
    if (allGuests < 2) {
        jQuery('.guest-quantity').html(allGuests + ' Guest');
    } else {
        jQuery('.guest-quantity').html(allGuests + ' Guests');
    }
    jQuery('input[name=allGuests]').val(allGuests);

}

function calculateGuestsMobile() {
    var adults = parseInt(jQuery('input[name=guest-adults]').val());
    var youth = parseInt(jQuery('input[name=guest-youth]').val());
    var allGuests = adults + youth;
    console.log(allGuests);
    if (allGuests < 2) {
        jQuery('.guest-quantity-mobile').html(allGuests + ' Guest');
    } else {
        jQuery('.guest-quantity-mobile').html(allGuests + ' Guests');
    }
    jQuery('input[name=allGuests]').val(allGuests);

}

function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[0] - 1, mdy[1]);
}

function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function formatDate(date) {
    var MyDateString;

    MyDateString = ('0' + (date.getMonth() + 1)).slice(-2) + '/' +
        ('0' + date.getDate()).slice(-2) + '/' +
        date.getFullYear();

    return MyDateString;
}

function showGallery(currentID) {
    var currentImage = jQuery('#image-' + currentID + ' img').data('full');
    jQuery('#galleryModal').modal('show');
    jQuery('#modalGallery').html('<img src="' + currentImage + '" class="img-fluid" data-current="' + currentID + '" />');
}

jQuery('#modalGalleryClose').on('click', function(e) {
    e.preventDefault();
    jQuery('#galleryModal').modal('hide');
});

jQuery('#modalGalleryPrev').on('click', function(e) {
    e.preventDefault();

    var currentID = jQuery('#modalGallery img').data('current');
    var newIDFull = jQuery('#image-' + currentID).prev().attr('id');
    if (newIDFull === undefined || newIDFull === null) {
        var newIDFull = jQuery('div.gallery-thumbs div:last-child').attr('id');
    }
    newID = newIDFull.split('-');
    var currentImage = jQuery('#image-' + newID[1] + ' img').data('full');

    jQuery('#modalGallery').html('');
    jQuery('#modalGallery').html('<img src="' + currentImage + '" class="img-fluid" data-current="' + newID[1] + '" />');
});

jQuery('#modalGalleryNext').on('click', function(e) {
    e.preventDefault();

    var currentID = jQuery('#modalGallery img').data('current');
    var newIDFull = jQuery('#image-' + currentID).next().attr('id');
    if (newIDFull === undefined || newIDFull === null) {
        var newIDFull = jQuery('div.gallery-thumbs div:first-child').attr('id');
    }
    newID = newIDFull.split('-');
    var currentImage = jQuery('#image-' + newID[1] + ' img').data('full');

    jQuery('#modalGallery').html('');
    jQuery('#modalGallery').html('<img src="' + currentImage + '" class="img-fluid" data-current="' + newID[1] + '" />');
});


function calculatePricing() {
    if (jQuery('#dateIn').val() == '') {
        return;
    }
    if (jQuery('#dateOut').val() == '') {
        return;
    }
    if ((jQuery('input[name=allGuests]').val() == '') || (jQuery('input[name=allGuests]').val() == 0)) {
        return;
    }

    jQuery('.date-guests-response').html('');

    var daysQty = parseInt(jQuery('input[name=daysQty]').val());
    var priceSingle = 0;
    var totalPrice = 0;

    /* GETTING PRICE BLOCKS */
    var priceBlocks = JSON.parse(jQuery('input[name=priceLocation]').val());

    console.log(priceBlocks);

    for (const [key, value] of Object.entries(priceBlocks)) {
        console.log(`${daysQty} ${key} ${value}`)
        if (key > daysQty) {
            break
        }

        priceSingle = value
    }
    console.log(`${priceSingle} * ${daysQty}`)
    console.log(priceSingle * daysQty);


    jQuery('input[name=priceSingle]').val(priceSingle);

    var cleaning_fee = parseFloat(jQuery('input[name=cleaning-fee]').val());
    var occupancy_taxes_and_fees = parseFloat(jQuery('input[name=occupancy-taxes-and-fees]').val());

    var subtotalPrice = daysQty * priceSingle;
    var subtotalTaxes = (subtotalPrice * occupancy_taxes_and_fees) / 100;
    var totalPrice = subtotalPrice + cleaning_fee + subtotalTaxes;

    jQuery('input[name=priceTotal]').val(totalPrice.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));



    jQuery('.main-table-item > .description-line').html(daysQty + ' x ' + priceSingle.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }) + ' per night');
    jQuery('.main-table-item > .price-line').html(subtotalPrice.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));
    jQuery('#occupancy-taxes').html(subtotalTaxes.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));

    jQuery('.total-table-item > .price-line').html(totalPrice.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));


    jQuery('#reviewOrder').removeClass('d-none');
    jQuery('.property-data-resume').addClass('d-none');
    jQuery('.btn-availability').addClass('d-none');
    jQuery('.btn-reservation').removeClass('d-none');
}

function calculatePricingMobile() {
    if (jQuery('#dateIn').val() == '') {
        return;
    }
    if (jQuery('#dateOut').val() == '') {
        return;
    }
    jQuery('.date-guests-response').html('');

    var daysQty = parseInt(jQuery('input[name=daysQty]').val());
    var priceSingle = 0;
    var totalPrice = 0;

    /* GETTING PRICE BLOCKS */
    var priceBlocks = JSON.parse(jQuery('input[name=priceLocation]').val());

    console.log(priceBlocks);

    for (const [key, value] of Object.entries(priceBlocks)) {
        console.log(`${daysQty} ${key} ${value}`)
        if (key > daysQty) {
            break
        }

        priceSingle = value
    }
    console.log(`${priceSingle} * ${daysQty}`);
    console.log(priceSingle * daysQty);


    jQuery('input[name=priceSingle]').val(priceSingle);

    var cleaning_fee = parseFloat(jQuery('input[name=cleaning-fee]').val());
    var occupancy_taxes_and_fees = parseFloat(jQuery('input[name=occupancy-taxes-and-fees]').val());

    var subtotalPrice = daysQty * priceSingle;
    var subtotalTaxes = (subtotalPrice * occupancy_taxes_and_fees) / 100;
    var totalPrice = subtotalPrice + cleaning_fee + subtotalTaxes;

    jQuery('input[name=priceTotal]').val(totalPrice.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));



    jQuery('.mobile-table-item > .description-line').html(daysQty + ' x ' + priceSingle.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }) + ' per night');
    jQuery('.mobile-table-item > .price-line').html(subtotalPrice.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));
    jQuery('#occupancy-taxes').html(subtotalTaxes.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));

    jQuery('.total-mobile-table-item > .price-line').html(totalPrice.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD',
    }));


    jQuery('.property-data-resume').addClass('d-none');
    jQuery('.btn-availability').addClass('d-none');
    jQuery('.btn-reservation').removeClass('d-none');
}

var locationSwiper = new Swiper('.swiper-location-gallery-mobile', {
    direction: 'horizontal',
    loop: true,
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
});

var bdetypeSwiper = new Swiper('.bed-type-swiper-container', {
    direction: 'horizontal',
    loop: true,
    slidesPerView: 3,
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        991: {
            slidesPerView: 3,
            spaceBetween: 40
        }
    }
});