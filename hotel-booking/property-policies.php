<h2 class="property-subtitle"><?php _e('Policies', 'yam'); ?></h2>
<div class="property-wrapper">
    <?php $item_data = get_post_meta(get_the_ID(), 'yam_location_policies_group', true); ?>
    <div class="approved-policies">
        <?php foreach ($item_data as $item) { ?>
        <?php if ($item['icon'] == 'check') { ?>
        <div class="single-item">
            <img src="<?php echo get_template_directory_uri(); ?>/images/icon-check-purple.png" alt="<?php echo $item['title']; ?>">
            <?php echo $item['title']; ?>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
    <div class="rejected-policies">
        <?php foreach ($item_data as $item) { ?>
        <?php if ($item['icon'] == 'cancel') { ?>
        <div class="single-item">
            <img src="<?php echo get_template_directory_uri(); ?>/images/icon-mark-purple.png" alt="<?php echo $item['title']; ?>">
            <?php echo $item['title']; ?>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
</div>
<h3><?php _e('Additional Rules', 'yam'); ?></h3>
<?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'yam_additional_rules', true)); ?>