<div class="property-wrapper">
    <div class="property-single-data">
        <div class="single-item">
            <?php $item_data = get_the_terms(get_the_ID(), 'mphb_room_type_category'); ?>
            <?php foreach ($item_data as $item) { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/house.png" alt="<?php echo $item->name; ?>"> <?php echo $item->name; ?>
            <?php } ?>
        </div>
        <div class="single-item">
            <?php $item = get_post_meta(get_the_ID(), 'yam_location_guests', true); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/guests.png" alt="<?php echo $item; ?>"> <?php echo $item; ?> <?php _e('guests', 'yam'); ?>
        </div>
        <div class="single-item">
            <?php $item = get_post_meta(get_the_ID(), 'yam_location_bedrooms', true); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/beds.png" alt="<?php echo $item; ?>"> <?php echo $item; ?> <?php _e('bedrooms', 'yam'); ?>
        </div>
        <div class="single-item">
            <?php $item = get_post_meta(get_the_ID(), 'yam_location_bathrooms', true); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/baths.png" alt="<?php echo $item; ?>"> <?php echo $item; ?> <?php _e('bathrooms', 'yam'); ?>
        </div>
        <div class="single-item">
            <?php $item = get_post_meta(get_the_ID(), 'yam_location_half_bathrooms', true); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/half_bathroom.png" alt="<?php echo $item; ?>"> <?php echo $item; ?> <?php _e('half-bathrooms', 'yam'); ?>
        </div>
        <div class="single-item">
            <?php $item = get_post_meta(get_the_ID(), 'yam_location_minimum_stay', true); ?>
            <?php $item2 = get_post_meta(get_the_ID(), 'yam_location_maximum_stay', true); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/min_stay.png" alt="<?php echo $item; ?>-<?php echo $item2; ?>"> <?php echo $item; ?>-<?php echo $item2; ?> <?php _e('nights minimun stay', 'yam'); ?>
        </div>
        <div class="single-item">
            <?php $item = get_post_meta(get_the_ID(), 'yam_location_special', true); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/locations/walking_distance_to_the_beach.png" alt="<?php echo $item; ?>"> <?php echo $item; ?>
        </div>
    </div>
    <div class="property-maps">
        <div class="embed-responsive embed-responsive-4by3">
            <?php echo get_post_meta(get_the_ID(), 'yam_location_map', true); ?>
        </div>
    </div>
</div>