<?php $posted_id = array(get_the_ID()); ?>
<h2 class="property-subtitle"><?php _e('Other Rentals...', 'yam'); ?></h2>
<div class="row">
    <?php $arr_locations = new WP_Query(array('post_type' => 'mphb_room_type', 'posts_per_page' => 2, 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => $posted_id)); ?>
    <?php if ($arr_locations->have_posts()) : ?>
    <?php while ($arr_locations->have_posts()) : $arr_locations->the_post(); ?>
    <article class="related-locations-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <picture>
            <a href="<?php the_permalink(); ?>" title="<?php _e('View this property', 'yam'); ?>">
                <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
            </a>
        </picture>
        <div class="meta-container">
            <div class="meta-item">
                <img src="<?php echo get_template_directory_uri(); ?>/images/locations/guests.png" alt="Guests" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_guests', true); ?> <?php _e('Guests', 'yam'); ?>
            </div>
            <div class="meta-item">
                <img src="<?php echo get_template_directory_uri(); ?>/images/locations/beds.png" alt="Beds" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bedrooms', true); ?> <?php _e('Bedrooms', 'yam'); ?>
            </div>
            <div class="meta-item">
                <img src="<?php echo get_template_directory_uri(); ?>/images/locations/baths.png" alt="Baths" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bathrooms', true); ?> <?php _e('Bathrooms', 'yam'); ?>
            </div>
        </div>
        <a href="<?php the_permalink(); ?>" title="<?php _e('View this property', 'yam'); ?>">
            <h2><?php the_title(); ?></h2>
        </a>
        <div class="dir-container">
            <?php $item_data = get_the_terms(get_the_ID(), 'mphb_ra_location-address'); ?>
            <?php foreach ($item_data as $item) { ?>
            <div class="single-item">
                <?php echo $item->name; ?>
            </div>
            <?php } ?>
        </div>
        <div class="price-container">
            <?php $price = get_post_meta(get_the_ID(), 'yam_location_price', true); ?>
            <?php printf(__('Starting from US$%s per night', 'yam'), $price); ?>
        </div>
    </article>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_query(); ?>
</div>