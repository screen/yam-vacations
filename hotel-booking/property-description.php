<?php the_content(); ?>
<?php $video_url = get_post_meta(get_the_ID(), 'yam_location_video', true); ?>
<?php if ($video_url != '') { ?>
<?php $video_url = transform_embed_video($video_url); ?>
<div class="embed-responsive embed-responsive-16by9">
    <iframe src="<?php echo $video_url; ?>" frameborder="0" class="embed-responsive-item"></iframe>
</div>
<?php } ?>