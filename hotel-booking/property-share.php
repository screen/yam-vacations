<a id="shareLink" class="btn btn-md btn-link">
    <i class="fa fa-share-alt"></i>
</a>
<div id="shareMenu" class="share-menu share-menu-hidden">
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
    <a href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
    <a href="https://www.linkedin.com/shareArticle?url=<?php echo urlencode(get_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
    <a href="https://api.whatsapp.com/send?text=<?php echo urlencode(get_the_title()); ?> <?php echo urlencode(get_permalink()); ?>" target="_blank"><i class="fa fa-whatsapp"></i></a>
</div>