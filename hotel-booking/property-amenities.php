<h2 class="property-subtitle"><?php _e('Amenities', 'yam'); ?></h2>
<div class="property-wrapper">
    <?php $item_data = get_the_terms(get_the_ID(), 'mphb_room_type_facility'); ?>
    <?php foreach ($item_data as $item) { ?>
    <div class="single-item">
        <img src="<?php echo get_template_directory_uri(); ?>/images/icon-check-purple.png" alt="<?php echo $item->name; ?>"> <?php echo $item->name; ?>
    </div>
    <?php } ?>
</div>