<ul id="menu_ul_properties">
    <li id="overview-menu"><a href="#overview"><?php _e('Overview', 'yam'); ?></a></li>
    <li id="rooms-menu"><a href="#rooms"><?php _e('Room/Beds', 'yam'); ?></a></li>
    <li id="amenities-menu"><a href="#amenities"><?php _e('Amenities', 'yam'); ?></a></li>
    <li id="policies-menu"><a href="#policies"><?php _e('Policies', 'yam'); ?></a></li>
    <li id="pointinterest-menu"><a href="#pointinterest"><?php _e('Near by', 'yam'); ?></a></li>
    <li id="reviews-menu"><a href="#reviews"><?php _e('Reviews', 'yam'); ?></a></li>
</ul>
<div class="dropdown dropleft">
    <button class="btn dropdown-toggle btn-small" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="<?php echo get_template_directory_uri(); ?>/images/icon-chevron.png" alt="">
    </button>
    <div id="overflow" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a id="overview-over" class="dropdown-item d-none" href="#overview"><?php _e('Overview', 'yam'); ?></a>
        <a id="rooms-over" class="dropdown-item d-none" href="#rooms"><?php _e('Room/Beds', 'yam'); ?></a>
        <a id="amenities-over" class="dropdown-item d-none" href="#amenities"><?php _e('Amenities', 'yam'); ?></a>
        <a id="policies-over" class="dropdown-item d-none" href="#policies"><?php _e('Policies', 'yam'); ?></a>
        <a id="pointinterest-over" class="dropdown-item d-none" href="#pointinterest"><?php _e('Near by', 'yam'); ?></a>
        <a id="reviews-over" class="dropdown-item d-none" href="#reviews"><?php _e('Reviews', 'yam'); ?></a>
    </div>
</div>