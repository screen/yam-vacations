<?php
if (!defined('ABSPATH')) {
    exit;
}
?>

<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row  no-gutters">
        <div class="container container-special">
            <div class="row">
                <?php /* MAIN CONTENT */ ?>
                <div class="property-single col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                    <div class="row">
                        <div class="back-btn-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <a href="<?php echo home_url('/accommodation'); ?>" title="<?php _e('View all rentals', 'yam'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-chevron.png" alt="Go Back" class="img-fluid" /> <?php _e('View all rentals', 'yam'); ?></a>
                        </div>

                        <?php /* get_location_restrictions(get_the_ID()); */ ?>

                        <header class="property-header col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h1 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h1>
                            <div class="property-share property-share-mobile d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                <?php echo get_template_part('hotel-booking/property-share'); ?>
                            </div>
                            <div class="property-header-reviews">
                                <?php echo get_template_part('hotel-booking/property-reviews'); ?>
                            </div>
                        </header>
                        <?php echo get_template_part('hotel-booking/property-gallery'); ?>
                        <nav class="property-navigator col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-special-menu'); ?>
                        </nav>
                        <div id="#overview" class="property-overview col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-overview'); ?>
                        </div>
                        <div class="property-description col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-description'); ?>
                        </div>
                        <div id="rooms" class="property-room-beds col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-rooms'); ?>
                        </div>
                        <div id="amenities" class="property-amenities col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-amenities'); ?>
                        </div>
                        <div id="policies" class="property-policies col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-policies'); ?>
                        </div>
                        <div id="pointinterest" class="property-point-interest col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-points-interest'); ?>
                        </div>
                        <div id="reviews" class="property-reviews col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h2 class="property-subtitle"><?php _e('Reviews', 'yam'); ?></h2>
                            <div class="property-wrapper">
                                <?php if (comments_open()) { ?>
                                <?php comments_template(); ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="related-properties col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php echo get_template_part('hotel-booking/property-related'); ?>
                        </div>
                    </div>
                </div>
                <?php /* SIDEBAR */ ?>
                <div class="property-reservation property-reservation-mobile col-12 d-xl-none d-lg-none d-md-none d-sm-block d-block">
                    <div class="property-reservation-mobile-wrapper">
                        <div class="property-mobile-left-side">
                            <div class="property-price">$ <?php echo get_post_meta(get_the_ID(), 'yam_location_price', true); ?><small><?php _e('avg/night', 'yam'); ?></small></div>
                            <div class="property-price-reviews">
                                <?php echo get_template_part('hotel-booking/property-reviews'); ?>
                            </div>
                        </div>
                        <div class="property-mobile-right-side">
                            <button class="btn btn-md btn-availability-mobile"><?php _e('Check Availability') ?></button>
                            <button class="btn btn-md btn-reserve-mobile d-none"><?php _e('Reserve') ?></button>
                        </div>
                    </div>
                </div>

                <sidebar class="property-reservation col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <form id="mainReservationForm" class="sticker-data">
                        <?php $price_rates = get_location_rates_price(get_the_ID()); ?>
                        <?php $fees = get_option('mphb_fees'); ?>
                        <?php $taxes = get_option('mphb_fee_taxes'); ?>
                        <input type="hidden" name="propertyID" value="<?php echo get_the_ID(); ?>" />
                        <input type="hidden" name="daysQty" value="0" />
                        <input type="hidden" name="priceLocation" value='<?php echo json_encode($price_rates); ?>' />
                        <input type="hidden" name="priceSingle" value='' />
                        <input type="hidden" name="priceTotal" value="" />
                        <?php if (!empty($fees)) { ?>
                        <?php foreach ($fees as $item) { ?>
                        <input type="hidden" name="<?php echo sanitize_title($item['label']); ?>" value="<?php echo $item['amount']; ?>" />
                        <?php } ?>
                        <?php } ?>
                        <?php if (!empty($taxes)) { ?>
                        <?php foreach ($taxes as $item) { ?>
                        <input type="hidden" name="<?php echo sanitize_title($item['label']); ?>" value="<?php echo $item['amount']; ?>" />
                        <?php } ?>
                        <?php } ?>
                        <input type="hidden" name="allGuests" value="" />
                        <div class="sticker-data-wrapper">
                            <div class="sticker-data-row">
                                <div class="property-price">$ <?php echo get_post_meta(get_the_ID(), 'yam_location_price', true); ?><small><?php _e('avg/night', 'yam'); ?></small></div>
                                <div class="property-share d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                    <?php echo get_template_part('hotel-booking/property-share'); ?>
                                </div>
                            </div>
                            <div class="property-data-resume">
                                <div class="property-price-reviews">
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <span class="review-qty">(<?php echo get_comments_number(); ?> reviews)</span>
                                </div>
                                <div class="meta-container">
                                    <div class="meta-item">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/locations/guests.png" alt="Guests" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_guests', true); ?> <?php _e('Guests', 'yam'); ?>
                                    </div>
                                    <div class="meta-item">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/locations/beds.png" alt="Beds" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bedrooms', true); ?> <?php _e('Bedrooms', 'yam'); ?>
                                    </div>
                                    <div class="meta-item">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/locations/baths.png" alt="Baths" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bathrooms', true); ?> <?php _e('Bathrooms', 'yam'); ?>
                                    </div>
                                </div>

                                <div class="special-container">
                                    <?php $item = get_post_meta(get_the_ID(), 'yam_location_special', true); ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/locations/walking_distance_to_the_beach.png" alt="<?php echo $item; ?>"> <?php echo $item; ?>
                                </div>
                            </div>
                            <div class="main-reservation-form">
                                <div class="date-selector">
                                    <div id="dateInSelector" class="date-checkin" data-toggle="modal" data-target="#dateModal">
                                        <label for="dateIn"><?php _e('Check In', 'yam'); ?></label>
                                        <input id="dateIn" type="hidden" name="date-in" />
                                        <span id="dateInLabel" class="date">MM/DD/YYYY</span>
                                    </div>
                                    <div id="dateOutSelector" class="date-checkout" data-toggle="modal" data-target="#dateModal">
                                        <label for="dateOut"><?php _e('Check Out', 'yam'); ?></label>
                                        <input id="dateOut" type="hidden" name="date-out" />
                                        <span id="dateOutLabel" class="date">MM/DD/YYYY</span>
                                    </div>
                                </div>
                                <div class="guest-selector">
                                    <div id="guestSelector" class="guest-selector-wrapper">
                                        <label for="guest-quantity"><?php _e('Guests', 'yam'); ?></label>
                                        <input id="guestQty" type="hidden" name="guest-quantity" value="1" />
                                        <span class="guest-quantity"><?php _e('No guests selected', 'yam'); ?></span>
                                    </div>
                                    <div class="guest-selector-menu d-none">
                                        <div class="guest-selector-item">
                                            <div class="guest-label">
                                                <h4><?php _e('Adults', 'yam'); ?></h4>
                                            </div>

                                            <div class="guest-input-container">
                                                <span class="minus">-</span>
                                                <input type="number" name="guest-adults" min="0" value="0" />
                                                <span class="plus">+</span>
                                            </div>

                                        </div>
                                        <div class="guest-selector-item">
                                            <div class="guest-label">
                                                <h4><?php _e('Adults', 'yam'); ?></h4>
                                                <small><?php _e('Ages 2-12', 'yam'); ?></small>
                                            </div>

                                            <div class="guest-input-container">
                                                <span class="minus">-</span>
                                                <input type="number" name="guest-youth" min="0" value="0" />
                                                <span class="plus">+</span>
                                            </div>

                                        </div>
                                        <div class="guest-selector-item">
                                            <div class="guest-label">
                                                <h4><?php _e('Infants', 'yam'); ?></h4>
                                                <small><?php _e('Under 2', 'yam'); ?></small>
                                            </div>

                                            <div class="guest-input-container">
                                                <span class="minus">-</span>
                                                <input type="number" name="guest-infant" min="0" value="0" />
                                                <span class="plus">+</span>
                                            </div>
                                        </div>
                                        <div class="guest-selector-item">
                                            <small><?php _e("10 guests maximum. Infants don't count toward the number of guests.", "yam"); ?></small>
                                        </div>
                                    </div>
                                </div>
                                <div id="reviewOrder" class="table-order d-none">
                                    <div class="table-item main-table-item">
                                        <div class="description-line"></div>
                                        <div class="price-line"></div>
                                    </div>
                                    <?php foreach ($fees as $item) { ?>
                                    <div class="table-item">
                                        <div class="description-line"><?php echo $item['label']; ?></div>
                                        <div class="price-line">$ <?php echo number_format($item['amount'], 2); ?></div>
                                    </div>
                                    <?php } ?>
                                    <?php foreach ($taxes as $item) { ?>
                                    <div class="table-item">
                                        <div class="description-line"><?php echo $item['label']; ?></div>
                                        <div class="price-line" id="occupancy-taxes"><?php echo number_format($item['amount'], 2); ?></div>
                                    </div>
                                    <?php } ?>
                                    <div class="table-item total-table-item">
                                        <div class="description-line"><?php _e('Total', 'yam'); ?></div>
                                        <div class="price-line"></div>
                                    </div>
                                </div>
                                <div class="date-guests-response"></div>
                                <button class="btn btn-md btn-reservation d-none"><?php _e('INQUIRE', 'yam'); ?></button>
                                <button class="btn btn-md btn-availability"><?php _e('Check Availability') ?></button>

                            </div>
                            <div class="special-message">
                                <?php /* ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon-free-cancel.png" alt="Free Cancellation" class="img-fluid"> <?php _e('Free cancellation up to 60 days before check-in'); ?>
                                <?php */ ?>
                            </div>
                            <div class="mail-message">
                                <a href="mailto:admin@screen.com.ve"><?php _e('Ask owner a question'); ?></a>
                            </div>
                        </div>
                        <div class="confirm-reservation d-none">
                            <h2><?php _e('Confirm Availability', 'yam'); ?></h2>
                            <div class="confirm-input-wrapper">
                                <input type="text" name="firstName" class="form-control reservation-control" placeholder="<?php _e('First Name', 'yam'); ?>">
                                <small id="errorReservationName" class="danger error d-none"></small>
                            </div>
                            <div class="confirm-input-wrapper">
                                <input type="text" name="lastName" class="form-control reservation-control" placeholder="<?php _e('Last Name', 'yam'); ?>">
                                <small id="errorReservationName" class="danger error d-none"></small>
                            </div>
                            <div class="confirm-input-wrapper">
                                <input type="email" name="EmailAddress" class="form-control reservation-control" placeholder="<?php _e('Email Address', 'yam'); ?>">
                                <small id="errorReservationName" class="danger error d-none"></small>
                            </div>
                            <div class="confirm-input-wrapper">
                                <input type="text" name="phone" class="form-control reservation-control" placeholder="<?php _e('Phone', 'yam'); ?>">
                                <small id="errorReservationName" class="danger error d-none"></small>
                            </div>
                            <div class="confirm-input-wrapper">
                                <input type="text" name="location" class="form-control reservation-control" placeholder="<?php _e('Where are you From?', 'yam'); ?>">
                                <small id="errorReservationName" class="danger error d-none"></small>
                            </div>
                            <div class="confirm-input-wrapper">
                                <textarea name="comments" id="comments" class="form-control reservation-control" placeholder="<?php _e('Comments', 'yam'); ?>"></textarea>
                                <small id="errorReservationName" class="danger error d-none"></small>
                            </div>
                            <?php $google_settings = get_option('yam_google_settings'); ?>
                            <?php if (isset($google_settings['sitekey'])) { ?>
                            <div class="g-recaptcha" data-sitekey="<?php echo $google_settings['sitekey']; ?>"></div>
                            <small id="errorRecaptcha" class="invalid-control d-none"><?php _e('you must validate this recatpcha', 'yam'); ?></small>
                            <?php } ?>
                            <?php $sendinblue_settings = get_option('yam_sendinblue_settings'); ?>
                            <input type="hidden" name="listID" id="listID" value="<?php echo $sendinblue_settings['reservation_list_id']; ?>">
                            <button class="btn btn-md btn-complete-form"><?php _e('Make Reservation') ?></button>
                        </div>
                    </form>
                </sidebar>
            </div>
        </div>
    </div>
</main>

<!-- MODAL -->
<div class=" modal modal-date fade" id="dateModal" tabindex="-1" aria-labelledby="dateModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body modal-body-dates">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-date-header">
                    <div class="modal-date-title">
                        <h3><?php _e('Select Dates', 'yam'); ?></h3>
                        <span><?php printf(__('Minimum stay: %s nights', 'yam'), get_post_meta(get_the_ID(), 'yam_location_minimum_stay', true)); ?></span>
                    </div>
                    <div class="date-selected d-none">
                        <div class="date-selected-in">
                            <label for=""><?php _e('Check In', 'yam'); ?></label>
                            <span id="modalDateIn" class="modal-date date">MM/DD/YYYY</span>
                        </div>
                        <div class="date-selected-out">
                            <label for=""><?php _e('Check Out', 'yam'); ?></label>
                            <span id="modalDateOut" class="modal-date date">MM/DD/YYYY</span>
                        </div>
                    </div>
                </div>
                <div class="modal-date-content">
                    <?php $min_date = get_post_meta(get_the_ID(), 'yam_location_minimum_stay', true); ?>
                    <?php $max_date = get_post_meta(get_the_ID(), 'yam_location_maximum_stay', true); ?>
                    <?php $max_date = 40 ?>
                    <div class="modal-date-container">
                        <input id="demo" />
                    </div>
                </div>
                <div class="modal-response-container d-none"></div>
                <div class="modal-date-closer">
                    <button class="btn btn-md btn-save"><?php _e('Select these dates', 'yam'); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class=" modal modal-gallery fade" id="galleryModal" tabindex="-1" aria-labelledby="dateModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body modal-body-gallery">
                <div id="modalGallery">

                </div>
                <div class="modal-close">
                    <button id="modalGalleryClose"><i class="fa fa-close"></i></button>
                </div>
                <div class="modal-arrows modal-arrow-prev">
                    <button id="modalGalleryPrev"><i class="fa fa-chevron-left"></i></button>
                </div>
                <div class="modal-arrows modal-arrow-next">
                    <button id="modalGalleryNext"><i class="fa fa-chevron-right"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class=" modal modal-mobile fade" id="mobileModal" tabindex="-1" aria-labelledby="dateModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body modal-body-gallery">
                <div class="mobile-reservation-form">
                    <div class="mobile-reservation-title">
                        <h3><?php _e('Your Trip', 'yam'); ?></h3>
                        <small><?php _e('Reservation Summary', 'yam'); ?></small>
                    </div>
                    <div class="mobile-reservation-price-container">
                        <div class="mobile-reservation-price-element">
                            <div class="property-price">$ <?php echo get_post_meta(get_the_ID(), 'yam_location_price', true); ?><small><?php _e('avg/night', 'yam'); ?></small></div>
                        </div>
                        <div class="mobile-reservation-price-element property-mobile-reviews">
                            <?php echo get_template_part('hotel-booking/property-reviews'); ?>
                        </div>
                    </div>
                    <div class="date-selector">
                        <div id="dateInMobileSelector" class="date-checkin">
                            <label for="dateIn"><?php _e('Check In', 'yam'); ?></label>
                            <input id="dateInMobile" type="hidden" name="date-in" />
                            <span id="dateInLabelMobile" class="date">MM/DD/YYYY</span>
                        </div>
                        <div id="dateOutMobileSelector" class="date-checkout">
                            <label for="dateOut"><?php _e('Check Out', 'yam'); ?></label>
                            <input id="dateOutMobile" type="hidden" name="date-out" />
                            <span id="dateOutLabelMobile" class="date">MM/DD/YYYY</span>
                        </div>
                    </div>
                    <div class="guest-selector">
                        <div id="guestSelectorMobile" class="guest-selector-wrapper">
                            <label for="guest-quantity"><?php _e('Guests', 'yam'); ?></label>
                            <span class="guest-quantity guest-quantity-mobile"><?php _e('No guests selected', 'yam'); ?></span>
                        </div>
                        <div class="guest-selector-menu d-none">
                            <div class="guest-selector-item">
                                <div class="guest-label">
                                    <h4><?php _e('Adults', 'yam'); ?></h4>
                                </div>

                                <div class="guest-input-container">
                                    <span class="minus">-</span>
                                    <input type="number" name="guest-adults" min="0" value="0" />
                                    <span class="plus">+</span>
                                </div>

                            </div>
                            <div class="guest-selector-item">
                                <div class="guest-label">
                                    <h4><?php _e('Adults', 'yam'); ?></h4>
                                    <small><?php _e('Ages 2-12', 'yam'); ?></small>
                                </div>

                                <div class="guest-input-container">
                                    <span class="minus">-</span>
                                    <input type="number" name="guest-youth" min="0" value="0" />
                                    <span class="plus">+</span>
                                </div>

                            </div>
                            <div class="guest-selector-item">
                                <div class="guest-label">
                                    <h4><?php _e('Infants', 'yam'); ?></h4>
                                    <small><?php _e('Under 2', 'yam'); ?></small>
                                </div>

                                <div class="guest-input-container">
                                    <span class="minus">-</span>
                                    <input type="number" name="guest-infant" min="0" value="0" />
                                    <span class="plus">+</span>
                                </div>
                            </div>
                            <div class="guest-selector-item">
                                <small><?php _e("10 guests maximum. Infants don't count toward the number of guests.", "yam"); ?></small>
                            </div>
                        </div>
                    </div>
                    <div id="reviewOrder" class="table-order">
                        <div class="table-item main-table-item mobile-table-item">
                            <div class="description-line"></div>
                            <div class="price-line"></div>
                        </div>
                        <?php foreach ($fees as $item) { ?>
                        <div class="table-item">
                            <div class="description-line"><?php echo $item['label']; ?></div>
                            <div class="price-line">$ <?php echo number_format($item['amount'], 2); ?></div>
                        </div>
                        <?php } ?>
                        <?php foreach ($taxes as $item) { ?>
                        <div class="table-item">
                            <div class="description-line"><?php echo $item['label']; ?></div>
                            <div class="price-line" id="occupancy-taxes"><?php echo number_format($item['amount'], 2); ?></div>
                        </div>
                        <?php } ?>
                        <div class="table-item total-table-item total-mobile-table-item">
                            <div class="description-line"><?php _e('Total', 'yam'); ?></div>
                            <div class="price-line"></div>
                        </div>
                    </div>
                    <div class="date-guests-response"></div>
                    <button class="btn btn-md btn-reservation-mobile"><?php _e('INQUIRE', 'yam'); ?></button>
                </div>
                <div class="special-message">
                    <?php /* ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon-free-cancel.png" alt="Free Cancellation" class="img-fluid"> <?php _e('Free cancellation up to 60 days before check-in'); ?>
                    <?php */ ?>
                </div>
                <div class="mail-message">
                    <a href="mailto:admin@screen.com.ve"><?php _e('Ask owner a question'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>