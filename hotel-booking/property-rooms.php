<h2 class="property-subtitle"><?php _e('Rooms & Beds', 'yam'); ?></h2>
<div class="property-beds-wrapper">
    <div class="swiper-container bed-type-swiper-container">
        <div class="swiper-wrapper">
            <?php $bed_group = get_post_meta(get_the_ID(), 'yam_location_bedtype_group', true); ?>
            <?php if (!empty($bed_group) || ($bed_group != '')) { ?>
            <?php $i = 1; ?>
            <?php foreach ($bed_group as $item) { ?>
            <div class="swiper-slide">
                <div class="property-bed-type-item">
                    <?php $image_name = ($item['type'] == 'single') ? 'single_bed.png' : 'double_bed.png'; ?>
                    <div class="image-wrapper">
                        <?php for ($y=0; $y < $item['quantity']; $y++) { ?>
                        <img src="<?php  echo get_template_directory_uri(); ?>/images/locations/<?php echo $image_name; ?>" alt="<?php echo $item['type']; ?>" class="img-fluid" />
                        <?php } ?>
                    </div>
                    <h3><?php _e('Bedroom', 'yam'); ?> <?php echo $i; ?></h3>
                    <?php $type_bed = ($item['quantity'] > 1) ? $item['type'] . ' beds' : $item['type'] . ' bed'; ?>
                    <span><?php echo $item['quantity']; ?> <?php echo $type_bed; ?></span>
                </div>
            </div>
            <?php $i++; } ?>
            <?php } ?>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>