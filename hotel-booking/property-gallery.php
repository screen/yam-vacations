<?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
<picture class="property-gallery col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
    <div class="row">
        <div class="gallery-main col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
            <?php $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'main_image'); ?>
            <?php $post_full = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
            <?php $bg_banner_id = get_post_thumbnail_id(get_the_ID()); ?>
            <img id="main-<?php echo $bg_banner_id; ?>" onclick="showGallery(<?php echo $bg_banner_id; ?>);" itemprop="image" data-full="<?php echo $post_full; ?>" content="<?php echo $post_thumbnail; ?>" src="<?php echo $post_thumbnail; ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>" class="img-fluid" />
        </div>
        <div class="gallery-thumbs col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
            <?php $gallery_images = get_post_meta(get_the_ID(), 'mphb_gallery', true); ?>
            <?php $i = 1; ?>
            <?php $gallery_images = explode(',', $gallery_images); ?>
            <?php $count_gallery = (count($gallery_images) - 3); ?>

            <?php foreach ($gallery_images as $item) { ?>
            <?php $bg_banner_id = $item; ?>

            <?php if ($i == 1) { ?>
            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'boxed', false); ?>
            <?php $bg_banner_full = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
            <div id="image-<?php echo $bg_banner_id; ?>" class="gallery-thumb-item gallery-thumb-image-big">
                <img onclick="showGallery(<?php echo $bg_banner_id; ?>);" itemprop="image" data-full="<?php echo $bg_banner_full[0]; ?>" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
            </div>
            <?php } ?>

            <?php if (($i >= 2) && ($i <= 3)) { ?>
            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'small', false); ?>
            <?php $bg_banner_full = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
            <div id="image-<?php echo $bg_banner_id; ?>" class="gallery-thumb-item gallery-thumb-image-small">
                <img onclick="showGallery(<?php echo $bg_banner_id; ?>);" itemprop="image" data-full="<?php echo $bg_banner_full[0]; ?>" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                <?php if ($i == 3) { ?>
                <div class="number">+<?php echo $count_gallery; ?></div>
                <?php } ?>
            </div>
            <?php } ?>

            <?php if ($i > 4) { ?>
            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'small', false); ?>
            <?php $bg_banner_full = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
            <div id="image-<?php echo $bg_banner_id; ?>" class="gallery-thumb-item gallery-thumb-image-small d-none">
                <img onclick="showGallery();" itemprop="image" data-full="<?php echo $bg_banner_full[0]; ?>" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
            </div>
            <?php } ?>
            <?php $i++; } ?>

            <?php /* REPEAT POST THUMBNAIL FOR GALLERY */ ?>
            <?php $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'main_image'); ?>
            <?php $post_full = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
            <?php $bg_banner_id = get_post_thumbnail_id(get_the_ID()); ?>
            <div id="image-<?php echo $bg_banner_id; ?>" class="gallery-thumb-item gallery-thumb-image-small d-none">
                <img onclick="showGallery(<?php echo $bg_banner_id; ?>);" itemprop="image" data-full="<?php echo $post_full; ?>" content="<?php echo $post_thumbnail; ?>" src="<?php echo $post_thumbnail; ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>" class="img-fluid" />
            </div>

        </div>
    </div>
</picture>
<picture class="property-gallery col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-none d-lg-none d-md-none d-sm-block d-block">
    <div class="swiper-container swiper-location-gallery-mobile">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <?php the_post_thumbnail('large', $defaultatts); ?>
            </div>
            <?php foreach ($gallery_images as $item) { ?>
            <?php $bg_banner_id = $item; ?>
            <div class="swiper-slide">
                <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'large', false); ?>
                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
            </div>
            <?php } ?>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</picture>