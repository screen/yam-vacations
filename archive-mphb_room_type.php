<?php get_header(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <div class="page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row">
                    <div class="title-container archive-main-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1><?php _e('WE HAVE THE BEST LOCATIONS IN MIAMI', 'yam'); ?></h1>
                    </div>
                    <?php if (have_posts()) : ?>
                    <section class="archive-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
                        <?php $found_posts = $wp_query->post_count; ?>
                        <?php $i = 1; ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="archive-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 <?php echo join(' ', get_post_class()); ?>" role="article">
                            <div class="row align-items-center">
                                <?php $class_info = ($i%2 == 0) ? 'order-xl-12 order-lg-12 order-md-12 order-sm-12 order-12' : 'order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12'; ?>
                                <?php $class_image = ($i%2 == 0) ? 'order-xl-1 order-lg-1 order-md-1 order-sm-1 order-1' : 'order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1'; ?>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 <?php echo $class_info; ?>">
                                    <header>
                                        <div class="archive-location">
                                            <?php $arr_locations = get_the_terms( get_the_ID(), 'mphb_ra_location-address'); ?>
                                            <?php if (!empty($arr_locations)) : ?>
                                            <?php foreach ($arr_locations as $item) { ?>
                                            <span><?php echo $item->name; ?></span>
                                            <?php } ?>
                                            <?php endif; ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                                        </a>
                                        <div class="meta-container">
                                            <div class="meta-item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/locations/guests.png" alt="Guests" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_guests', true); ?> <?php _e('Guests', 'yam'); ?>
                                            </div>
                                            <div class="meta-item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/locations/beds.png" alt="Beds" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bedrooms', true); ?> <?php _e('Bedrooms', 'yam'); ?>
                                            </div>
                                            <div class="meta-item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/locations/baths.png" alt="Baths" class="img-fluid" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bathrooms', true); ?> <?php _e('Bathrooms', 'yam'); ?>
                                            </div>
                                        </div>
                                    </header>
                                    <div class="archive-excerpt">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" title="<?php _e('View Available Dates', 'yam'); ?>" class="btn btn-md btn-archive"><?php _e('View Available Dates', 'yam'); ?></a>
                                </div>
                                <picture class="archive-images col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12 <?php echo $class_image; ?>">
                                    <div class="archive-images-wrapper">
                                        <div class="archive-post-thumbnail">
                                            <?php if ( has_post_thumbnail()) : ?>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <?php the_post_thumbnail('archive_location_big', $defaultatts); ?>
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="archive-post-gallery">
                                            <?php $y = 1; ?>
                                            <?php $gallery_img = get_post_meta(get_the_ID(), 'mphb_gallery', true); ?>
                                            <?php $gallery_img = explode(',', $gallery_img); ?>
                                            <?php foreach ($gallery_img as $item) { ?>
                                            <?php $bg_banner_id = $item; ?>
                                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'archive_location_small', false); ?>
                                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                            <?php $y++; ?>
                                            <?php if ($y > 3) { break; } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </picture>
                            </div>
                        </article>
                        <?php if ($found_posts > $i) { ?>
                        <div class="archive-divider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/divider-wave.png" alt="Divider" class="img-fluid" />
                        </div>
                        <?php } ?>
                        <?php $i++; endwhile; ?>
                        <div class="pagination col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                        </div>
                    </section>
                    <?php else: ?>
                    <section class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php _e('Disculpe, su busqueda no arrojo ningun resultado', 'yam'); ?></h2>
                        <h3><?php _e('Dirígete nuevamente al', 'yam'); ?> <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'yam'); ?>"><?php _e('inicio', 'yam'); ?></a>.</h3>
                    </section>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>