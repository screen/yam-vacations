<?php

/**
 * Template Name: Home Page
 *
 * @package yam
 * @subpackage yam-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="home-main-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php $arr_slides = get_post_meta(get_the_ID(), 'yam_slider_images', true); ?>
            <?php if (!empty($arr_slides)) : ?>
            <div class="home-swiper-container swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($arr_slides as $key => $value) { ?>
                    <div class="swiper-slide">
                        <?php $image = wp_get_attachment_image_src($key, 'full'); ?>
                        <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="home-main-slider-wrapper">
                <h1><?php echo get_post_meta(get_the_ID(), 'yam_slider_title', true); ?></h1>
                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'yam_slider_desc', true)); ?>
                <a href="<?php echo get_post_meta(get_the_ID(), 'yam_slider_btn_link', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'yam_slider_btn_text', true); ?>" class="btn btn-md btn-slider"><?php echo get_post_meta(get_the_ID(), 'yam_slider_btn_text', true); ?></a>
            </div>

            <?php $arr_logos = get_post_meta(get_the_ID(), 'yam_slider_logos', true); ?>
            <?php if (!empty($arr_logos)) : ?>
            <div class="home-main-slider-logos-container">
                <?php foreach ($arr_logos as $key => $value) { ?>
                <?php $image = wp_get_attachment_image_src($key, 'full'); ?>
                <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                <?php } ?>
            </div>
            <?php endif; ?>
            <?php endif; ?>
        </section>

        <section class="home-video-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row">
                    <?php $arr_locations = new WP_Query(array('post_type' => 'mphb_room_type', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'meta_query' => array(
                        array(
                            'key'     => 'yam_location_highlight',
                            'value'   => 'on',
                            'compare' => '=',
                        ),
                    ))); ?>
                    <?php if ($arr_locations->have_posts()) : ?>
                    <?php while ($arr_locations->have_posts()) : $arr_locations->the_post(); ?>
                    <?php $posted_id = array(get_the_ID()); ?>
                    <article class="home-locations-item-featured col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="home-locations-item-wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/bigplay.png" title="<?php _e('View Video', 'yam'); ?>" alt="Play" class="img-fluid img-play" data-video="<?php echo transform_embed_video(get_post_meta(get_the_ID(), 'yam_location_video', true)); ?>" />
                            <picture>
                                <?php the_post_thumbnail('full', array('class' => 'img-fluid d-xl-block d-lg-block d-md-block d-sm-none d-none')); ?>

                                <?php the_post_thumbnail('home_location', array('class' => 'img-fluid d-xl-none d-lg-none d-md-none d-sm-block d-block')); ?>
                            </picture>
                            <a class="content" href="<?php the_permalink(); ?>" title="<?php _e('View location', 'yam'); ?>">
                                <div class="home-locations-item-subtitle">
                                    <?php _e('Exclusive Listing', 'yam'); ?>
                                </div>
                                <h2><?php the_title(); ?></h2>
                                <div class="meta-container">
                                    <span><?php echo get_post_meta(get_the_ID(), 'yam_location_bedrooms', true); ?> <?php _e('Beds', 'yam'); ?></span>
                                    <span><?php echo get_post_meta(get_the_ID(), 'yam_location_bathrooms', true); ?> <?php _e('Baths', 'yam'); ?></span>
                                    <span><?php echo get_post_meta(get_the_ID(), 'mphb_size', true); ?> sq ft</span>
                                    <?php $price = get_post_meta(get_the_ID(), 'yam_location_price', true); ?>
                                    <span><?php printf(__('$%s/night', 'yam'), $price); ?></span>
                                </div>
                            </a>
                        </div>
                    </article>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </section>

        <section class="home-benefits-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row align-items-center">
                    <div class="home-benefits-content col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-12 order-sm-12 order-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'yam_home_benefits_content', true)); ?>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'yam_benefits_btn_link', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'yam_benefits_btn_text', true); ?>" class="btn btn-md btn-benefits"><?php echo get_post_meta(get_the_ID(), 'yam_benefits_btn_text', true); ?></a>
                    </div>
                    <div class="home-benefits-picture col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 prder-xl-12 order-lg-12 order-md-1 order-sm-1 order-1">
                        <?php $image = wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'yam_home_benefits_image_id', true), 'full'); ?>
                        <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                    </div>
                </div>
            </div>
        </section>

        <section class="home-locations-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row">
                    <?php $arr_locations = new WP_Query(array('post_type' => 'mphb_room_type', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => array($posted_id))); ?>
                    <?php if ($arr_locations->have_posts()) : ?>
                    <?php while ($arr_locations->have_posts()) : $arr_locations->the_post(); ?>
                    <article class="home-locations-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="home-locations-item-wrapper">
                            <picture>
                                <?php the_post_thumbnail('home_location', array('class' => 'img-fluid')); ?>
                            </picture>
                            <a class="content" href="<?php the_permalink(); ?>" title="<?php _e('View location', 'yam'); ?>">
                                <div class="meta-container">
                                    <div class="meta-item">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/locations/guests.png" alt="Guests" class="img-fluid img-invert" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_guests', true); ?> <?php _e('Guests', 'yam'); ?>
                                    </div>
                                    <div class="meta-item">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/locations/beds.png" alt="Beds" class="img-fluid img-invert" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bedrooms', true); ?> <?php _e('Bedrooms', 'yam'); ?>
                                    </div>
                                    <div class="meta-item">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/locations/baths.png" alt="Baths" class="img-fluid img-invert" /> <?php echo get_post_meta(get_the_ID(), 'yam_location_bathrooms', true); ?> <?php _e('Bathrooms', 'yam'); ?>
                                    </div>
                                </div>
                                <h2><?php the_title(); ?></h2>
                                <div class="dir-container">
                                    <?php $item_data = get_the_terms(get_the_ID(), 'mphb_ra_location-address'); ?>
                                    <?php foreach ($item_data as $item) { ?>
                                    <div class="single-item">
                                        <?php echo $item->name; ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="price-container">
                                    <?php $price = get_post_meta(get_the_ID(), 'yam_location_price', true); ?>
                                    <?php printf(__('Starting from US$%s per night', 'yam'), $price); ?>
                                </div>
                            </a>
                        </div>
                    </article>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <div class="view-more-locations col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo home_url('/accommodation'); ?>" class="btn btn-md btn-view-more"><?php _e('View all rentals', 'yam'); ?></a>
                    </div>
                </div>
            </div>
        </section>



        <section class="home-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row align-items-center">
                    <div class="home-about-picture col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                        <?php $arr_slides = get_post_meta(get_the_ID(), 'yam_home_about_slider_images', true); ?>
                        <?php if (!empty($arr_slides)) : ?>
                        <div class="about-swiper-container swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($arr_slides as $key => $value) { ?>
                                <div class="swiper-slide">
                                    <?php $image = wp_get_attachment_image_src($key, 'full'); ?>
                                    <img loading="lazy" itemprop="logo" content="<?php echo $image[0]; ?>" src="<?php echo $image[0]; ?>" alt="" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="home-about-content col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'yam_home_about_content', true)); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- Modal -->
<div class="modal modal-home fade" id="homeVideoModal" tabindex="-1" aria-labelledby="homeVideoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-fluid">
                    <div class="row">
                        <div class="video-modal-content col-12">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="" id="embedModalVideo" class="embed-responsive-item"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>