<form id="partnersForm" class="partners-form-container" data-aos="fade" data-aos-delay="500">
    <div class="row">
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formName" name="form-name" type="text" class="form-control input-form-control" placeholder="<?php _e('First Name', 'yam'); ?>">
            <small id="errorName" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formLast" name="form-last" type="text" class="form-control input-form-control" placeholder="<?php _e('Last Name', 'yam'); ?>">
            <small id="errorLast" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formCompany" name="form-company" type="text" class="form-control input-form-control" placeholder="<?php _e('Company', 'yam'); ?>">
            <small id="errorCompany" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formType" name="form-type" type="text" class="form-control input-form-control" placeholder="<?php _e('Type of service (i.e. Yoga classes, Boat, Rental, Chef)', 'yam'); ?>">
            <small id="errorType" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formEmail" name="form-email" type="email" class="form-control input-form-control" placeholder="<?php _e('Email', 'yam'); ?>">
            <small id="errorEmail" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formPhone" name="form-phone" type="text" class="form-control input-form-control" placeholder="<?php _e('Phone', 'yam'); ?>">
            <small id="errorPhone" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <textarea id="formComments" name="form-comments" type="text" class="form-control input-form-control" placeholder="<?php _e('Comment', 'yam'); ?>"></textarea>
            <small id="errorComments" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item submit-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        </div>
        <div class="input-field-item submit-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="subscribeFormBtn" type="submit" class="btn btn-md btn-form"><?php _e('Submit', 'yam'); ?></button>
            <div class="loader-css d-none"></div>
        </div>
    </div>
</form>