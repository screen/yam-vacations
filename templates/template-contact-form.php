<form id="subscribeForm" class="subscribe-form-container" data-aos="fade" data-aos-delay="500">
    <div class="input-field-item">
        <input id="formName" name="form-name" type="text" class="form-control input-form-control" placeholder="<?php _e('Full Name', 'yam'); ?>">
        <small id="errorName" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item">
        <input id="formEmail" name="form-email" type="email" class="form-control input-form-control" placeholder="<?php _e('Email', 'yam'); ?>">
        <small id="errorEmail" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item">
        <input id="formPhone" name="form-phone" type="text" class="form-control input-form-control" placeholder="<?php _e('Phone', 'yam'); ?>">
        <small id="errorPhone" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item">
        <textarea id="formComments" name="form-comments" type="text" class="form-control input-form-control" placeholder="<?php _e('Comment', 'yam'); ?>"></textarea>
        <small id="errorComments" class="invalid-control d-none"></small>
    </div>
    <div class="input-field-item">
        <?php $google_settings = get_option('yam_google_settings'); ?>
        <?php if (isset($google_settings['sitekey'])) { ?>
        <div class="g-recaptcha" data-sitekey="<?php echo $google_settings['sitekey']; ?>"></div>
        <small id="errorRecaptcha" class="invalid-control d-none"><?php _e('you must validate this recatpcha', 'yam'); ?></small>
        <?php } ?>
        <?php $sendinblue_settings = get_option('yam_sendinblue_settings'); ?>
        <input type="hidden" name="listID" id="listID" value="<?php echo $sendinblue_settings['footer_list_id']; ?>">
    </div>

    <div class="input-field-item submit-field-item">
        <button id="subscribeFormBtn" type="submit" class="btn btn-md btn-form"><?php _e('Submit', 'yam'); ?></button>
        <div class="loader-css footer-loader-css d-none"></div>
        <div class="footer-form-response"></div>
    </div>
</form>