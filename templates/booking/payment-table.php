<div class="no-break">
    <table class="rental-summary">
        <tbody>
            <tr>
                <td>Rental rate of</td>
                <td class="money-column">{rental-price}</td>
            </tr>
            <tr>
                <td>Security deposit <br>
                    <span class="font-small">*Deposit will be refund to guest if all is ok at check out</span>
                </td>
                <td class="money-column">{security-deposit}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="total-column">Total Amount Due</td>
                <td class="money-column">{total-amount-due}</td>
            </tr>
        </tfoot>
    </table>
</div>