<h3 class="text-center">SIGNATURES</h3>
<table class="signatures-table">
    <tbody>
        <tr>
            <td class="table-cell-half text-center px-3">
                <div class="placeholder">&nbsp;</div>
                <hr>
                <strong>Owner</strong> Signature
            </td>
            <td class="table-cell-half text-center px-3">
                <div class="placeholder">&nbsp;</div>
                <hr>
                <strong>Guest</strong> Signature
            </td>
        </tr>
        <tr>
            <td class="table-cell-half text-center px-3">
                <div class="placeholder">{owner-name}</div>
                <hr>
                <strong>Owner</strong> name
            </td>
            <td class="table-cell-half text-center px-3">
                <div class="placeholder">{guest-name}</div>
                <hr>
                <strong>Guest</strong> name
            </td>
        </tr>
        <tr>
            <td class="table-cell-half text-center px-3">
                <div class="placeholder">&nbsp;</div>
                <hr>
                Date
            </td>
            <td class="table-cell-half text-center px-3">
                <div class="placeholder">&nbsp;</div>
                <hr>
                Date
            </td>
        </tr>
    </tbody>
</table>