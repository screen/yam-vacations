<?php

/**
 * Template Name: Partner With Us [Form]
 *
 * @package yam
 * @subpackage yam-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section id="post-<?php the_ID(); ?>" class="page-container page-partners col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <div class="section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                    <div class="form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo get_template_part('templates/template-partner-form'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>