<form id="quoteForm" class="container-fluid" data-aos="fade" data-aos-delay="500">
    <div class="row">
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formName" name="form-name" type="text" class="form-control input-form-control" placeholder="<?php _e('First Name', 'yam'); ?>">
            <small id="errorName" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formLast" name="form-last" type="text" class="form-control input-form-control" placeholder="<?php _e('Last Name', 'yam'); ?>">
            <small id="errorLast" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formPhone" name="form-phone" type="text" class="form-control input-form-control" placeholder="<?php _e('Phone', 'yam'); ?>">
            <small id="errorPhone" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formEmail" name="form-email" type="email" class="form-control input-form-control" placeholder="<?php _e('Email', 'yam'); ?>">
            <small id="errorEmail" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formType" name="form-type" type="text" class="form-control input-form-control" placeholder="<?php _e('Type of service (i.e. Yoga classes, Boat, Rental, Chef)', 'yam'); ?>">
            <small id="errorType" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formQuantity" name="form-quantity" type="number" class="form-control input-form-control" placeholder="<?php _e('Number of People', 'yam'); ?>">
            <small id="errorQuantity" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formStartDate" name="form-start-date" type="date" class="form-control input-form-control" placeholder="<?php _e('From Date', 'yam'); ?>">
            <small id="errorStartDate" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input id="formEndDate" name="form-end-date" type="date" class="form-control input-form-control" placeholder="<?php _e('To Daste', 'yam'); ?>">
            <small id="errorEndDate" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <textarea id="formComments" name="form-comments" type="text" class="form-control input-form-control" placeholder="<?php _e('Any Special Requests?', 'yam'); ?>"></textarea>
            <small id="errorComments" class="invalid-control d-none"></small>
        </div>
        <div class="input-field-item submit-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        </div>
        <div class="input-field-item submit-field-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="quoteFormBtn" type="submit" class="btn btn-md btn-form"><?php _e('Submit', 'yam'); ?></button>
            <div class="loader-css d-none"></div>
        </div>
    </div>
</form>