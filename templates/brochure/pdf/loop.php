<article>

    {contact-info}

    <header class="property-header">
        <h1 title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h1>
        <div class="property-reviews">
            <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" alt="star">
            <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" alt="star">
            <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" alt="star">
            <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" alt="star">
            <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" alt="star">
            (16 reviews)
        </div>
    </header>

    <div class="property-thumbnail">
        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large') ?>" width="600" height="auto" />
    </div>

    <h2>Property Overview</h2>
    <p><b>Location:</b> {property_location}</p>
    <p><b>Special:</b> {property_special}</p>

    <div class="features-list">
        <table>
            <tbody>
                <tr>
                    <td class="feature-list__item"><img src="<?php echo get_template_directory_uri(); ?>/images/locations/house.png" alt="Hotel"> {property_category}</td>
                    <td class="feature-list__item"><img src="<?php echo get_template_directory_uri(); ?>/images/locations/guests.png" alt="Hotel"> {property_guests} Guests</td>
                </tr>
                <tr>
                    <td class="feature-list__item"><img src="<?php echo get_template_directory_uri(); ?>/images/locations/beds.png" alt="5"> {property_bedrooms} Bedrooms</td>
                    <td class="feature-list__item"><img src="<?php echo get_template_directory_uri(); ?>/images/locations/baths.png" alt="3"> {property_bathrooms} Bathrooms</td>
                </tr>
                <tr>
                    <td class="feature-list__item"><img src="<?php echo get_template_directory_uri(); ?>/images/locations/half_bathroom.png" alt="3"> {property_half_bathrooms} Half-Bathrooms</td>
                    <td class="feature-list__item"><img src="<?php echo get_template_directory_uri(); ?>/images/locations/min_stay.png" alt="2-7"> {property_minimum} - {property_maximum} Minimum Stay</td>
                </tr>
            </tbody>
        </table>
    </div>

    <?php the_content() ?>

    <h2>Property Amenities</h2>
    <div class="features-list">
        <table>
            <tbody>
                <?php
                $item_data = get_the_terms(get_the_ID(), 'mphb_room_type_facility');
                $tr = true;
                foreach ($item_data as $item) {
                    if ($tr) {
                        echo '<tr>';
                    }
                ?>
                    <td class="feature-list__item">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon-check-purple.png" alt="check" />
                        <span class="feature-list__item-label"><?php echo $item->name ?></span>
                    </td>
                <?php
                    if (!$tr) {
                        echo '</tr>';
                    }

                    $tr = !$tr;
                }

                if (!$tr) {
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <h2>Property Policies</h2>
    <div class="features-list">
        <table>
            <tbody>
                <?php
                $item_data = get_post_meta(get_the_ID(), 'yam_location_policies_group', true);
                $tr = true;
                foreach ($item_data as $item) {
                    if ($tr) {
                        echo '<tr>';
                    }
                ?>
                    <td class="feature-list__item">
                        <?php if (strpos($item['title'], 'No ') !== false) { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon-mark-purple.png" alt="mark" />
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon-check-purple.png" alt="check" />
                        <?php } ?>
                        <span class="feature-list__item-label"><?php echo $item['title']; ?></span>
                    </td>
                <?php
                    if (!$tr) {
                        echo '</tr>';
                    }

                    $tr = !$tr;
                }

                if (!$tr) {
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <h2>Additional Rules</h2>
    {property_additional_rules}

    <h2>Near By</h2>
    <p>{property_nearby}</p>

    <?php get_template_part('templates/brochure/pdf/gallery') ?>

</article>