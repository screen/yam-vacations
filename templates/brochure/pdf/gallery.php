<?php
$thumbnail_id = get_post_meta(get_the_ID(), '_thumbnail_id', true);
$gallery_ids = get_post_meta(get_the_ID(), 'mphb_gallery', true);

$ids = [];

if ($thumbnail_id) {
    $ids[] = $thumbnail_id;
}

if ($gallery_ids) {
    $ids = array_merge($ids, explode(',', $gallery_ids));
}

if (count($ids) <= 1) {
    return;
}

$gallery = get_posts([
    'post__in' => $ids,
    'posts_per_page' => -1,
    'post_type' =>  'attachment',
    'post_status' => 'any'
]);

if (count($gallery) <= 1) {
    return;
}

$left = true;
?>

<div class="page-break"></div>

<h2>Property gallery</h2>

<table class="property-gallery">
    <?php foreach ($gallery as $image) { ?>
        <?php
        $bg_image = wp_get_attachment_image_src($image->ID, 'large');
        $class = $left ? 'property-gallery__item--left' : 'property-gallery__item--right';
        if ($left) {
            echo '<tr>';
        }
        ?>
        <td>
            <div class="property-gallery__item <?php echo $class ?>" style="background-image: url('<?php echo $bg_image[0] ?>');">&nbsp;</div>
        </td>
        <?php
        if (!$left) {
            echo '</tr>';
        }
        $left = !$left;
        ?>
    <?php } ?>
    <?php
    if (!$left) {
        echo '</tr>';
    }
    $left = !$left;
    ?>
</table>