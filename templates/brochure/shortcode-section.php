<?php
if (empty($args['posts'])) {
    return false;
}

global $post;
?>
<div class="container-fluid brochure-section">
    <div class="row justify-content-center">
        <?php
        foreach ($args['posts'] as $post) {
            setup_postdata($post);

            // Omit if the acommodation don't have thumbnail
            if (!has_post_thumbnail()) {
                continue;
            }

            get_template_part('templates/brochure/shortcode-loop');
        } ?>
    </div>
    <div class="row">
        <div class="col-12 my-5">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#get-brochure-modal" data-id="all">
                Download all accommodations
            </button>

        </div>
    </div>
</div>
<?php
wp_reset_postdata();
