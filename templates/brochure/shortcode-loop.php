<div class="col-lg-4 col-md-6 p-0 brochure-section__loop">
    <a href="#" data-toggle="modal" data-target="#get-brochure-modal" data-id="<?php the_id() ?>">
        <div class="brochure-section__loop__image">
            <?php the_post_thumbnail('medium'); ?>
        </div>
        <div class="brochure-section__loop__title">
            <?php the_title(); ?>
        </div>
    </a>
</div>