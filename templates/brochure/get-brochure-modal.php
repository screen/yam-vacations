<!-- Modal -->
<div class="modal fade" id="get-brochure-modal" tabindex="-1" role="dialog" aria-labelledby="get-brochure-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-brochure" role="document">
        <form class="modal-content" method="GET" action="<?php echo admin_url('admin-ajax.php') ?>">
            <div class="modal-header">
                <h5 class="modal-title" id="get-brochure-modalLabel">Insert your contact info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <textarea id="contact-info" name="contact-info" class="w-100 form-control" rows="10"></textarea>
                <input type="hidden" name="id" value="">
                <input type="hidden" name="action" value="create_report_by_location">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Generate PDF</button>
            </div>
        </form>
    </div>
</div>

<script>
    jQuery('#get-brochure-modal').on('show.bs.modal', function(event) {
        let button = jQuery(event.relatedTarget)
        let id = button.data('id')
        let modal = jQuery(this)
        modal.find("input[name=id]").val(id)
    })
</script>