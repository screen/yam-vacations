<?php
/**
* Template Name: Thanks Reservation
*
* @package yam
* @subpackage yam-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section id="post-<?php the_ID(); ?>" class="thanks-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="row">
                <div class="section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/thanks-icon.png" alt="Thanks" class="img-fluid">
                    <?php the_content(); ?>
                    <a href="<?php echo home_url('/'); ?>" title="<?php _e('go back to home', 'yam'); ?>" class="btn btn-md btn-back-home"><?php _e('go back to home', 'yam'); ?></a>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>