<?php

/**
 * Template Name: Concierge Services
 *
 * @package yam
 * @subpackage yam-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section id="post-<?php the_ID(); ?>" class="page-concierge col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container container-special">
                <div class="row">
                    <div class="section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="menu-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="menu-concierges">
                <?php $arr_group = get_post_meta(get_the_ID(), 'yam_concierge_group', true); ?>
                <?php if (!empty($arr_group)) : ?>
                    <ul id="menu_ul">
                        <?php foreach ($arr_group as $item) { ?>
                            <?php $title_slug = str_replace(' ', '_', $item['title']); ?>
                            <li id="<?php echo strtolower($title_slug); ?>-menu">
                                <a href="#<?php echo strtolower($title_slug); ?>"><?php echo $item['title']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="dropdown dropleft">
                        <button class="btn dropdown-toggle btn-small" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon-chevron.png" alt="">
                        </button>
                        <div id="overflow" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <?php foreach ($arr_group as $item) { ?>
                                <?php $title_slug = str_replace(' ', '_', $item['title']); ?>
                                <a id="<?php echo strtolower($title_slug); ?>-over" class="dropdown-item d-none" href="#<?php echo strtolower($title_slug); ?>"><?php echo $item['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <section class="main-concierge-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container container-special">
                <div class="row">
                    <?php $i = 1; ?>
                    <?php if (!empty($arr_group)) : ?>
                        <?php foreach ($arr_group as $item) { ?>
                            <?php $title_slug = str_replace(' ', '_', $item['title']); ?>
                            <?php if ($i % 2 == 0) { ?>
                                <article id="<?php echo strtolower($title_slug); ?>" class="main-concierge-item even col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="row align-items-center">
                                        <header class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12">
                                            <h3 class="title"><?php echo $item['title']; ?></h3>
                                            <div class="content">
                                                <?php echo apply_filters('the_content', $item['content']); ?>
                                            </div>
                                            <div class="button">
                                                <?php $modal = ($item['modal'] == 'on') ? 'data-toggle="modal" data-target="#quoteModal"' : ''; ?>
                                                <a href="<?php echo $item['link_url']; ?>" title="<?php echo $item['link_text']; ?>" class="btn btn-md btn-concierge" <?php echo $modal; ?>><?php echo $item['link_text']; ?></a>
                                            </div>
                                        </header>
                                        <picture class="item-picture col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1">
                                            <?php $bg_banner_id = $item['image_id']; ?>
                                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'logo', false); ?>
                                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                        </picture>
                                    </div>
                                </article>
                            <?php } else { ?>
                                <article id="<?php echo strtolower($title_slug); ?>" class="main-concierge-item even col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="row align-items-center">
                                        <picture class="item-picture col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <?php $bg_banner_id = $item['image_id']; ?>
                                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                        </picture>
                                        <header class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <h3 class="title"><?php echo $item['title']; ?></h3>
                                            <div class="content">
                                                <?php echo apply_filters('the_content', $item['content']); ?>
                                            </div>
                                            <div class="button">
                                                <?php $modal = ($item['modal'] == 'on') ? 'data-toggle="modal" data-target="#quoteModal"' : ''; ?>
                                                <a href="<?php echo $item['link_url']; ?>" title="<?php echo $item['link_text']; ?>" class="btn btn-md btn-concierge" <?php echo $modal; ?>><?php echo $item['link_text']; ?></a>
                                            </div>
                                        </header>
                                    </div>
                                </article>


                            <?php } ?>
                        <?php $i++;
                        } ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <!-- Modal -->
        <div class="modal fade" id="quoteModal" tabindex="-1" aria-labelledby="quoteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="quoteModalLabel"><?php _e('Request a Luxury Concierge Service Quote', 'yam'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo get_template_part('templates/template-quote-form'); ?>
                    </div>
                    <div class="modal-footer">
                        <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-phone.png" alt="phone"> 305.527.2367</span>
                        <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-email.png" alt="email"> shiraencaoua@gmail.com</span>
                        <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-instagram.png" alt="instagram"> </span>
                        <span><img src="<?php echo get_template_directory_uri(); ?>/images/icon-quote-facebook.png" alt="facebook"> </span>
                        <span>@yamvacation</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>